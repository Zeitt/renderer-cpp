#include <Application.hpp>

#include <GLFW/glfw3.h>

#include <iostream>

int main(int argc, char* argv[])
{
	using namespace renderer;

	struct GLFWData
	{
		glm::vec2 windowResolution{1280, 720};
		glm::vec2 windowScale{1, 1};
		bool damaged{true};
	} glfwData{};

	if (glfwInit() != GLFW_TRUE)
	{
		std::cerr << "GLFW3 initialization failed!\n";
		return -1;
	}

	if (!glfwVulkanSupported())
	{
		std::cerr << "Vulkan is not available!\n";
		return -2;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	GLFWwindow* window =
		glfwCreateWindow(glfwData.windowResolution.x, glfwData.windowResolution.y, "Vulkan Renderer", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cerr << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -3;
	}
	glfwWaitEventsTimeout(1.0); // Wait for X11 window refresh
	glfwSetWindowUserPointer(window, &glfwData);
	glfwSetWindowSizeCallback(window, [](GLFWwindow* window, int width, int height) {
		auto data = (GLFWData*) glfwGetWindowUserPointer(window);
		data->windowResolution = {float(width), float(height)};
		data->damaged = true;
		glfwPollEvents();
	});
	glfwSetWindowContentScaleCallback(window, [](GLFWwindow* window, float ws, float hs) {
		auto data = (GLFWData*) glfwGetWindowUserPointer(window);
		data->windowScale = {ws, hs};
		data->damaged = true;
		glfwPollEvents();
	});
	glfwGetWindowContentScale(window, &glfwData.windowScale.x, &glfwData.windowScale.y);

	double time = glfwGetTime();
	Application app{window};
	while (!glfwWindowShouldClose(window))
	{
		glfwPollEvents();
		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		{
			glfwSetWindowShouldClose(window, true);
		}
		else
		{
			if (glfwData.damaged)
			{
				app.recreateSwapchain();
				glfwData.damaged = false;
			}

			double temp = glfwGetTime();
			// If render returns true surface is damaged
			if (app.render(temp - time))
			{
				glfwWaitEventsTimeout(1.f / 60.f);
			}
			time = temp;
		}
	}

	glfwTerminate();
	return 0;
}