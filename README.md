# Renderer

Toy renderer using Vulkan. 

**Only x64 Linux with X11 actively tested!** (Tagged releases are also checked on Windows)

![progress](./progress.jpg)

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Even though project uses [volk](https://github.com/zeux/volk) for loading Vulkan-functions dynamically, [VulkanSDK](https://vulkan.lunarg.com/sdk/home) (1.1 or greater) is also required (On some systems "vulkan-headers"-package might be enough). 

STL with C++17 support and [GLFW](https://www.glfw.org) compatible windowing library is also needed.

Project files are generated with [CMake](https://cmake.org).

### Building

CMakes build process fairly simple two step process:

1. Generate project files. e.g.
	`cmake . -Bbuild`
2. Build with CMake
	`cmake --build build -- -j{COMPILE_THREADS}`

### Installing & Testing

Neither automatic installation or testing has been implemented. 

## Contributing

No pull requests are currently accepted, if you have suggestion; send me message on [twitter](https://twitter.com/Zeitt) or open an issue.

## Versioning

[SemVer](http://semver.org/) will be used for versioning. However first minor version, 0.1, is still not done.

## License

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE) file for details

## Acknowledgements

This project uses following libraries:

Loading models: [Assimp](http://www.assimp.org)

Windowing system: [GLFW](https://www.glfw.org)

Math: [glm](https://glm.g-truc.net)

Shader compilation: [glslang](https://github.com/KhronosGroup/glslang), [shaderc](https://github.com/google/shaderc), [SPIRV-Cross](https://github.com/KhronosGroup/SPIRV-Cross), [-Reflect](https://github.com/chaoticbob/SPIRV-Reflect), [-Tools](https://github.com/KhronosGroup/SPIRV-Tools)

Image loading (and writing): [stb](https://github.com/nothings/stb/)

Vulkan: [volk](https://github.com/zeux/volk) & [Vulkan Memory Allocator](https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/index.html)
