#include <Device.hpp>
#include <Shader.hpp>

#include <shaderc/shaderc.hpp>
#include <spirv_reflect.h>

#include <cassert>
#include <fstream>
#include <iostream>

namespace renderer
{
std::vector<uint32_t> getSpirv(const std::string& path, shaderc_shader_kind kind)
{
	std::vector<uint32_t> data;

	shaderc::CompileOptions compileOptions{};
	compileOptions.SetSourceLanguage(shaderc_source_language_glsl);
	compileOptions.SetGenerateDebugInfo();
	compileOptions.SetNanClamp(true);
	compileOptions.SetTargetEnvironment(shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_1);
	compileOptions.SetOptimizationLevel(shaderc_optimization_level_performance);

	std::fstream file{path, std::ios::in | std::ios::ate};
	if (!file.is_open() && file.tellg() > 0)
	{
		std::cerr << "Failed to open shader file!\n";
		assert(false);
		return {};
	}
	std::string src;
	src.resize(file.tellg());
	file.seekg(0);
	file.read(src.data(), src.length());
	file.close();

	shaderc::Compiler compiler{};
	shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(src, kind, path.c_str(), compileOptions);
	if (result.GetCompilationStatus() != shaderc_compilation_status_success)
	{
		std::cerr << "Shader compilation failed:\n" << result.GetErrorMessage() << std::endl;
		assert(false);
		return {};
	}
	data.assign(result.begin(), result.end());
	return data;
}

VkResult getLayouts(const std::vector<uint32_t>& spirv, std::vector<Shader::DescriptorSetLayout>& layouts,
					Shader::Stage_t stage)
{
	SpvReflectShaderModule module{};
	const SpvReflectResult res = spvReflectCreateShaderModule(spirv.size() * sizeof(uint32_t), spirv.data(), &module);
	if (res != SpvReflectResult::SPV_REFLECT_RESULT_SUCCESS)
	{
		std::cerr << "spvReflect failed!\n";
		assert(false);
		return VK_ERROR_INITIALIZATION_FAILED;
	}
	if (module.descriptor_set_count > Shader::MAX_SETS)
	{
		std::cerr << "Too many descriptor sets!\n";
		assert(false);
		return VK_ERROR_TOO_MANY_OBJECTS;
	}
	for (uint32_t setIndex = 0; setIndex < module.descriptor_set_count; ++setIndex)
	{
		const SpvReflectDescriptorSet& reflectSet{module.descriptor_sets[setIndex]};
		layouts[reflectSet.set].resize(reflectSet.binding_count);
		for (uint32_t bindingIndex = 0; bindingIndex < reflectSet.binding_count; ++bindingIndex)
		{
			const SpvReflectDescriptorBinding* reflectBinding{reflectSet.bindings[bindingIndex]};
			layouts[reflectSet.set][reflectBinding->binding].stages |= stage;
			switch (reflectBinding->descriptor_type)
			{
			case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLER:
				layouts[reflectSet.set][reflectBinding->binding].type = VK_DESCRIPTOR_TYPE_SAMPLER;
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
				layouts[reflectSet.set][reflectBinding->binding].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
				layouts[reflectSet.set][reflectBinding->binding].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				break;
				/*
			case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
				break;
			case SPV_REFLECT_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
				break;
				 */
			default:
				std::cerr << "Unimplemented descriptor type!\n";
				assert(false);
				return VK_ERROR_UNKNOWN;
			}
		}
	}
	return VK_SUCCESS;
}

util::Result<Shader, VkResult> Device::createShader(const char* vertexFilePath, const char* fragmentFilePath)
{
	Shader shader{};
	shader.mLayouts.resize(Shader::MAX_SETS);

	VkResult res{VK_SUCCESS};

	const std::vector<uint32_t> vertexData = getSpirv(vertexFilePath, shaderc_glsl_vertex_shader);
	if (vertexData.empty())
	{
		std::cerr << "Failed to get SPIRV for Vertex shader!\n";
		assert(false);
		return util::Result<Shader, VkResult>(VK_ERROR_INITIALIZATION_FAILED);
	}
	{
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = vertexData.size() * sizeof(uint32_t);
		createInfo.pCode = reinterpret_cast<const uint32_t*>(vertexData.data());
		res = mTable.vkCreateShaderModule(mDevice, &createInfo, nullptr, &shader.mVertex);
		if (res != VK_SUCCESS)
		{
			std::cerr << "Failed to create Vertex Shader module!\n";
			assert(false);
			return util::Result<Shader, VkResult>(res);
		}
	}
	res = getLayouts(vertexData, shader.mLayouts, Shader::VERTEX_STAGE);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to get layouts from Vertex shader!\n";
		assert(false);
		return util::Result<Shader, VkResult>(res);
	}

	const std::vector<uint32_t> fragmentData = getSpirv(fragmentFilePath, shaderc_glsl_fragment_shader);
	if (fragmentData.empty())
	{
		std::cerr << "Failed to get SPIRV for Fragment shader!\n";
		assert(false);
		return util::Result<Shader, VkResult>(VK_ERROR_INITIALIZATION_FAILED);
	}
	{
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = fragmentData.size() * sizeof(uint32_t);
		createInfo.pCode = reinterpret_cast<const uint32_t*>(fragmentData.data());
		res = mTable.vkCreateShaderModule(mDevice, &createInfo, nullptr, &shader.mComputeFragment);
		if (res != VK_SUCCESS)
		{
			std::cerr << "Failed to create Fragment Shader module!\n";
			assert(false);
			return util::Result<Shader, VkResult>(res);
		}
	}
	res = getLayouts(fragmentData, shader.mLayouts, Shader::FRAGMENT_STAGE);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to get layouts from Fragment shader!\n";
		assert(false);
		return util::Result<Shader, VkResult>(res);
	}

	for (auto& layout : shader.mLayouts)
	{
		std::vector<VkDescriptorSetLayoutBinding> bindings(layout.size());
		for (size_t index{0}; index < bindings.size(); ++index)
		{
			VkDescriptorSetLayoutBinding& out = bindings[index];
			const auto& in = layout[index];
			out.binding = index;
			out.descriptorCount = 1;
			out.descriptorType = layout[index].type;
			if (in.stages & Shader::VERTEX_STAGE)
			{
				out.stageFlags |= VK_SHADER_STAGE_VERTEX_BIT;
			}
			if (in.stages & Shader::FRAGMENT_STAGE)
			{
				out.stageFlags |= VK_SHADER_STAGE_FRAGMENT_BIT;
			}
			if (in.stages & Shader::COMPUTE_STAGE)
			{
				out.stageFlags |= VK_SHADER_STAGE_COMPUTE_BIT;
			}
		}

		VkDescriptorSetLayout descriptorSetLayout{VK_NULL_HANDLE};
		VkDescriptorSetLayoutCreateInfo layoutInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
		layoutInfo.bindingCount = bindings.size();
		layoutInfo.pBindings = bindings.data();
		res = mTable.vkCreateDescriptorSetLayout(mDevice, &layoutInfo, nullptr, &descriptorSetLayout);
		if (res != VK_SUCCESS)
		{
			std::cerr << "VkDescriptorSetLayout creation failed!\n";
			assert(false);
			return util::Result<Shader, VkResult>(res);
		}
		shader.mVkLayouts.push_back(descriptorSetLayout);
	}

	return util::Result<Shader, VkResult>(shader);
}

void Device::destroyShader(Shader& shader)
{
	for (VkDescriptorSetLayout layout : shader.mVkLayouts)
	{
		mTable.vkDestroyDescriptorSetLayout(mDevice, layout, nullptr);
	}
	mTable.vkDestroyShaderModule(mDevice, shader.mComputeFragment, nullptr);
	if (shader.mVertex != VK_NULL_HANDLE)
	{
		mTable.vkDestroyShaderModule(mDevice, shader.mVertex, nullptr);
	}
}

const std::vector<VkDescriptorSetLayout>& Shader::getNativeLayouts() const
{
	return mVkLayouts;
}
VkDescriptorPoolSize Shader::getPoolSize(VkDescriptorType type) const
{
	VkDescriptorPoolSize size{};
	size.type = type;
	for (const DescriptorSetLayout& layout : mLayouts)
	{
		for (const Binding& binding : layout)
		{
			if (binding.type == type)
			{
				size.descriptorCount++;
			}
		}
	}

	return size;
}
} // namespace renderer