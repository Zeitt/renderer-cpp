#include <Context.hpp>

#include <GLFW/glfw3.h>

#include <cassert>
#include <iostream>
#include <vector>

namespace renderer
{
VkBool32 debugUtilsMessengerCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
									 VkDebugUtilsMessageTypeFlagsEXT messageTypes,
									 const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
{
	const std::string msg = pCallbackData->pMessage;
	if (msg.find("Device Extension:") == 0 || msg.find("Instance Extension:") == 0)
	{
		// lets skip these :P
		return VK_FALSE;
	}
	std::string type{};
	if (messageTypes & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT)
	{
		type += "[General]";
	}
	if (messageTypes & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT)
	{
		type += "[Validation]";
	}
	if (messageTypes & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT)
	{
		type = "[Performance]";
	}


	if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
	{
		std::cerr << "vkDebug: [Error] " << type << " \"" << msg << "\"" << std::endl;
	}
	else
	{
		std::string level{};
		if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
		{
			level = "[Warning] ";
		}
		else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
		{
			level = "[Info] ";
		}
		else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
		{
			level = "[Verbose] ";
		}
		std::cout << "vkDebug: " << level << type << " \"" << msg << "\"" << std::endl;
	}
	return VK_FALSE;
}

util::Result<Context, VkResult> Context::Create(GLFWwindow* window, const char* applicationName)
{
	auto res = volkInitialize();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to load Vulkan functions!\n";
		Context::Check(res);
		assert(false);
		return util::Result<Context, VkResult>(res);
	}

	Context ctx{};

	res = ctx.initializeInstance(applicationName);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Instance initialization failed!\n";
		Context::Check(res);
		assert(false);
		return util::Result<Context, VkResult>(res);
	}
#if defined(RENDERER_DEF_LINUX) || defined(RENDERER_DEF_WINDOWS)
	res = glfwCreateWindowSurface(ctx.mInstance, window, nullptr, &ctx.mSurface);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Surface creation failed!\n";
		Context::Check(res);
		assert(false);
		return util::Result<Context, VkResult>(res);
	}
#else
#error "Windowing system not implemented for platform"
#endif
	return util::Result<Context, VkResult>(ctx);
}

void Context::destroy()
{
	if (mInstance != VK_NULL_HANDLE)
	{
		if (mMessenger != VK_NULL_HANDLE)
		{
			vkDestroyDebugUtilsMessengerEXT(mInstance, mMessenger, nullptr);
		}
		if (mSurface != VK_NULL_HANDLE)
		{
			vkDestroySurfaceKHR(mInstance, mSurface, nullptr);
		}
		vkDestroyInstance(mInstance, VK_NULL_HANDLE);
	}
	mSurface = VK_NULL_HANDLE;
	mInstance = VK_NULL_HANDLE;
}

void Context::Check(VkResult result)
{
	if (result != VK_SUCCESS)
	{
		std::string error{};
#define CASE(X)     \
	case X:         \
		error = #X; \
		break;
		switch (result)
		{
			CASE(VK_NOT_READY)
			CASE(VK_TIMEOUT)
			CASE(VK_EVENT_SET)
			CASE(VK_EVENT_RESET)
			CASE(VK_INCOMPLETE)
			CASE(VK_ERROR_OUT_OF_HOST_MEMORY)
			CASE(VK_ERROR_OUT_OF_DEVICE_MEMORY)
			CASE(VK_ERROR_INITIALIZATION_FAILED)
			CASE(VK_ERROR_DEVICE_LOST)
			CASE(VK_ERROR_MEMORY_MAP_FAILED)
			CASE(VK_ERROR_LAYER_NOT_PRESENT)
			CASE(VK_ERROR_EXTENSION_NOT_PRESENT)
			CASE(VK_ERROR_FEATURE_NOT_PRESENT)
			CASE(VK_ERROR_INCOMPATIBLE_DRIVER)
			CASE(VK_ERROR_TOO_MANY_OBJECTS)
			CASE(VK_ERROR_FORMAT_NOT_SUPPORTED)
			CASE(VK_ERROR_FRAGMENTED_POOL)
			CASE(VK_ERROR_UNKNOWN)
			CASE(VK_ERROR_OUT_OF_POOL_MEMORY)
			CASE(VK_ERROR_INVALID_EXTERNAL_HANDLE)
			CASE(VK_ERROR_FRAGMENTATION)
			CASE(VK_ERROR_SURFACE_LOST_KHR)
			CASE(VK_ERROR_NATIVE_WINDOW_IN_USE_KHR)
			CASE(VK_SUBOPTIMAL_KHR)
			CASE(VK_ERROR_OUT_OF_DATE_KHR)
			CASE(VK_ERROR_VALIDATION_FAILED_EXT)
			CASE(VK_ERROR_NOT_PERMITTED_EXT)
		default:
			error = "Unknown or extension";
		}
#undef CASE
		std::cerr << "Vulkan failed with: '" << error << "'\n";
		assert(false);
		std::exit(result);
	}
}

// Private functions
VkResult Context::initializeInstance(const char* applicationName)
{
	VkApplicationInfo applicationInfo{VK_STRUCTURE_TYPE_APPLICATION_INFO};
	applicationInfo.pEngineName = "Renderer";
	applicationInfo.apiVersion = VK_API_VERSION_1_1;
	applicationInfo.engineVersion = 0;
	applicationInfo.pApplicationName = applicationName;

	VkInstanceCreateInfo instanceCreateInfo{VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
#ifdef RENDERER_DEF_DEBUG
	VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo{VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT};
	debugCreateInfo.messageSeverity =
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
		VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	debugCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
								  VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
								  VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	debugCreateInfo.pfnUserCallback = &debugUtilsMessengerCallback;

	instanceCreateInfo.pNext = &debugCreateInfo;
#endif
	instanceCreateInfo.pApplicationInfo = &applicationInfo;
	std::vector<const char*> extensions{
#ifdef RENDERER_DEF_DEBUG
		VK_EXT_DEBUG_UTILS_EXTENSION_NAME
#endif
	};
#if defined(RENDERER_DEF_LINUX) || defined(RENDERER_DEF_WINDOWS)
	uint32_t glfwExtCount;
	const char** glfwExts = glfwGetRequiredInstanceExtensions(&glfwExtCount);
	for (uint32_t index = 0; index < glfwExtCount; ++index)
	{
		extensions.push_back(glfwExts[index]);
	}
#else
#error "Windowing system not implemented for platform"
#endif
	instanceCreateInfo.enabledExtensionCount = extensions.size();
	instanceCreateInfo.ppEnabledExtensionNames = extensions.data();
	const std::vector<const char*> layers = {
#ifdef RENDERER_DEF_DEBUG
		"VK_LAYER_KHRONOS_validation"
#endif
	};
	instanceCreateInfo.enabledLayerCount = layers.size();
	instanceCreateInfo.ppEnabledLayerNames = layers.data();

	auto res = vkCreateInstance(&instanceCreateInfo, VK_NULL_HANDLE, &mInstance);
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkCreateInstance failed!\n";
		assert(false);
		return res;
	}
	volkLoadInstance(mInstance);

#ifdef RENDERER_DEF_DEBUG
	res = vkCreateDebugUtilsMessengerEXT(mInstance, &debugCreateInfo, nullptr, &mMessenger);
	assert(res == VK_SUCCESS);
#endif
	return VK_SUCCESS;
}
} // namespace renderer
