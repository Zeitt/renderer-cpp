#include <CommandBufferPool.hpp>
#include <Device.hpp>

#include <cassert>
#include <iostream>

namespace renderer
{
// CommandBuffer

CommandBuffer::CommandBuffer(const CommandBuffer&) = default;

CommandBuffer::CommandBuffer(VolkDeviceTable& table, VkCommandBuffer cmd) : mTable(table), mCmdBuffer(cmd)
{
}

VkResult CommandBuffer::begin(const VkCommandBufferBeginInfo& info)
{
	return mTable.vkBeginCommandBuffer(mCmdBuffer, &info);
}
VkResult CommandBuffer::end()
{
	return mTable.vkEndCommandBuffer(mCmdBuffer);
}


void CommandBuffer::pipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask,
									VkDependencyFlags dependencyFlags,
									const std::vector<VkMemoryBarrier>& memoryBarriers,
									const std::vector<VkBufferMemoryBarrier>& bufferBarriers,
									const std::vector<VkImageMemoryBarrier>& imageBarriers)
{
	mTable.vkCmdPipelineBarrier(mCmdBuffer, srcStageMask, dstStageMask, dependencyFlags, memoryBarriers.size(),
								memoryBarriers.data(), bufferBarriers.size(), bufferBarriers.data(),
								imageBarriers.size(), imageBarriers.data());
}

void CommandBuffer::copyBufferToImage(Buffer& buffer, Image& image, const VkBufferImageCopy& region)
{
	vkCmdCopyBufferToImage(mCmdBuffer, buffer.native(), image.native(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
						   &region);
}

void CommandBuffer::beginRenderPass(const VkRenderPassBeginInfo& renderPassInfo, VkSubpassContents contents)
{
	mTable.vkCmdBeginRenderPass(mCmdBuffer, &renderPassInfo, contents);
}
void CommandBuffer::bindPipeline(VkPipelineBindPoint bindPoint, const Pipeline& pipeline)
{
	mTable.vkCmdBindPipeline(mCmdBuffer, bindPoint, pipeline.mPipeline);
}
void CommandBuffer::bindVertexBuffers(size_t firstBindPoint, const std::vector<Buffer>& buffers,
									  const std::vector<size_t>& offsets)
{
	std::vector<VkBuffer> vkBuffers;
	vkBuffers.resize(buffers.size());
	std::vector<VkDeviceSize> vkOffsets;
	vkOffsets.resize(buffers.size());
	for (size_t index = 0; index < vkBuffers.size(); ++index)
	{
		vkBuffers[index] = buffers[index].native();
		vkOffsets[index] = offsets[index];
	}
	mTable.vkCmdBindVertexBuffers(mCmdBuffer, firstBindPoint, vkBuffers.size(), vkBuffers.data(), vkOffsets.data());
}
void CommandBuffer::bindIndexBuffer(const Buffer& buffer, size_t offset, VkIndexType type)
{
	mTable.vkCmdBindIndexBuffer(mCmdBuffer, buffer.native(), offset, type);
}
void CommandBuffer::bindDescriptorSets(VkPipelineBindPoint pipelineBindPoint, const Pipeline& layout, size_t firstSet,
									   const std::vector<VkDescriptorSet>& sets,
									   const std::vector<uint32_t>& dynamicOffsets)
{
	mTable.vkCmdBindDescriptorSets(mCmdBuffer, pipelineBindPoint, layout.mLayout, firstSet, sets.size(), sets.data(),
								   dynamicOffsets.size(), dynamicOffsets.data());
}

void CommandBuffer::drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset,
								uint32_t firstInstance)
{
	mTable.vkCmdDrawIndexed(mCmdBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
}

void CommandBuffer::endRenderPass()
{
	mTable.vkCmdEndRenderPass(mCmdBuffer);
}

void CommandBuffer::blitImage(const Image& src, VkImageLayout srcLayout, const Image& dst, VkImageLayout dstLayout,
							  const std::vector<VkImageBlit>& blits, VkFilter filter)
{
	mTable.vkCmdBlitImage(mCmdBuffer, src.native(), srcLayout, dst.native(), dstLayout, blits.size(), blits.data(),
						  filter);
}

// CommandPool
util::Result<CommandPool, VkResult> Device::createCommandPool(size_t primaryCommandBufferCount,
															  VkCommandPoolCreateFlagBits flagBits /*= 0*/)
{
	CommandPool pool{};

	VkCommandPoolCreateInfo createInfo{VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO};
	createInfo.queueFamilyIndex = mQueueFamilyIndex;
	createInfo.flags = flagBits;

	VkResult res = mTable.vkCreateCommandPool(mDevice, &createInfo, nullptr, &pool.mPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "CommandPool creation failed!\n";
		assert(false);
		return util::Result<CommandPool, VkResult>(res);
	}

	std::vector<VkCommandBuffer> nativeBuffers(primaryCommandBufferCount);

	VkCommandBufferAllocateInfo allocInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO};
	allocInfo.commandPool = pool.mPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = primaryCommandBufferCount;
	res = mTable.vkAllocateCommandBuffers(mDevice, &allocInfo, nativeBuffers.data());
	if (res != VK_SUCCESS)
	{
		std::cerr << "Allocating command buffers failed!\n";
		assert(false);
		return util::Result<CommandPool, VkResult>(res);
	}
	for (VkCommandBuffer cmd : nativeBuffers)
	{
		pool.mBuffers.emplace_back(CommandBuffer(mTable, cmd));
	}

	return util::Result<CommandPool, VkResult>(pool);
}
void Device::destroyCommandPool(CommandPool& pool)
{
	std::vector<VkCommandBuffer> nativeBuffers;
	nativeBuffers.reserve(pool.mBuffers.size());
	for (CommandBuffer& cmd : pool.mBuffers)
	{
		nativeBuffers.push_back(cmd.mCmdBuffer);
	}
	mTable.vkFreeCommandBuffers(mDevice, pool.mPool, nativeBuffers.size(), nativeBuffers.data());
	pool.mBuffers.clear();
	mTable.vkDestroyCommandPool(mDevice, pool.mPool, nullptr);
}

std::optional<CommandBuffer> CommandPool::getCommandBuffer(size_t index)
{
	if (index < mBuffers.size())
	{
		return mBuffers[index];
	}
	std::cerr << "Error in getCommandBuffer: index out of range" << std::endl;
	assert(!"Index out of range");
	return std::nullopt;
}

VkResult Device::resetCommandPool(CommandPool& pool, VkCommandPoolResetFlags flags /*= 0*/)
{
	return mTable.vkResetCommandPool(mDevice, pool.mPool, flags);
}
} // namespace renderer