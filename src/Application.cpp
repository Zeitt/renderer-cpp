#include <Application.hpp>
#include <Shader.hpp>
#include <Utils.hpp>

#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

#include <cassert>
#include <chrono>
#include <iostream>
#include <thread>

namespace renderer
{
enum ExitCodes : int32_t
{
#include "../res/ExitCodes.txt"
};

Application::Application(GLFWwindow* window)
{
	// Vulkan initialization
	createContext(window);
	createDevice();
	createSwapchain();
	createUploadBuffer();
	createCommandPool();
	createShader();
	createRenderTargets();
	createGraphicsPipeline();
	createFramebuffers();
	createView();
	createDescriptors();
	createFencesAndSemas();

	// Loading model data
	loadScene();
	convertImageDataToImages();
	createChalet();
	createSampler();

	imageInitBarriers();

	record();
	mUBO.model = mChalet.model;

	mSceneData = {};

	mCurrentPipelineIndex = mSwapchain.getPipelineLength()-1;
}

Application::~Application()
{
	VkResult res = mDevice.waitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "DeviceWaitIdle failed during destructor!\n";
	}
	mDevice.destroyBuffer(mUBO.buffer);
	mDevice.destroySampler(mLinearSampler);
	mDevice.destroyBuffer(mChalet.vertices);
	mDevice.destroyBuffer(mChalet.indices);
	for (Image& img : mTextures)
	{
		mDevice.destroyImage(img);
	}
	mDevice.destroyDescriptorPool(mFragPool);
	mDevice.destroyDescriptorPool(mVertexPool);
	mDevice.destroyFramebuffer(mFBO);
	mDevice.destroyCommandPool(mCmdPool);
	mDevice.destroyPipeline(mPipeline);
	mDevice.destroyRenderPass(mRenderPass);
	for (Image& img : mDepthImages)
	{
		mDevice.destroyImage(img);
	}
	mDepthImages.clear();
	for (auto& img : mRenderTargetImages)
	{
		mDevice.destroyImage(img);
	}
	mRenderTargetImages.clear();
	mDevice.destroyShader(mTriangleShader);
	mDevice.destroyBuffer(mUploadBuffer);
	mDevice.destroySwapchain(mSwapchain);
	for (auto& fence : mFences)
	{
		mDevice.destroyFence(fence);
	}
	for (auto& sema : mPresentSemaphores)
	{
		mDevice.destroySemaphore(sema);
	}
	for (auto& sema : mSemaphores)
	{
		mDevice.destroySemaphore(sema);
	}
	mCtx.destroyDevice(mDevice);
	mCtx.destroy();
}

void Application::recreateSwapchain()
{
	for (VkFence& mFence : mFences)
	{
		VkFence fence = mFence;
		VkResult res = mDevice.getFenceStatus(fence);
		if (res == VK_SUCCESS || res == VK_NOT_READY)
		{
			res = mDevice.waitAndResetFence(fence);
			if (res != VK_SUCCESS)
			{
				std::cerr << "Device deadlocked during recreateSwapchain!\n";
				Context::Check(res);
				assert(false);
				std::exit(FENCE_WAIT_FAILED);
			}
		}
		auto newFence = mDevice.createFence();
		mDevice.destroyFence(fence);
		if(!newFence)
		{
			std::cerr << "Failed to create new fence for swapchain!\n";
			Context::Check(newFence.unwrapError());
			assert(false);
			std::exit(FENCE_CREATION_FAILED);
		}
		mFence = newFence.unwrap();
	}

	VkResult res = mDevice.queueWaitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Device deadlocked during recreateSwapchain!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_WAIT_FAILED);
	}
	res = mDevice.waitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Device deadlocked during recreateSwapchain!\n";
		Context::Check(res);
		assert(false);
		std::exit(DEVICE_WAIT_FAILED);
	}
	mDevice.destroySwapchain(mSwapchain);

	createSwapchain();

	res = mDevice.resetCommandPool(mCmdPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to reset commandpool during recreateSwapchain!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_POOL_RESET_FAILED);
	}

	std::vector<VkImageMemoryBarrier> barriers{};
	barriers.reserve(mSwapchain.getPipelineLength());

	for (size_t index = 0; index < mSwapchain.getPipelineLength(); ++index)
	{
		VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = 0;
		barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		barrier.srcQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.dstQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.image = mSwapchain.getImageRef(index).native();
		barrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

		barriers.push_back(barrier);
	}

	CommandBuffer cmd{mCmdPool.getCommandBuffer(0).value()};
	VkCommandBufferBeginInfo begin{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
	res = cmd.begin(begin);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to start CommandBuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_BUFFER_BEGIN_FAILED);
	}

	cmd.pipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_DEPENDENCY_BY_REGION_BIT,
						{}, {}, barriers);

	res = cmd.end();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to end swapchain image pipelineBarrier!\n";
		Context::Check(res);
		assert(false);
		std::exit(UPLOAD_CMD_FAILED);
	}

	std::vector<CommandBuffer> cmds{cmd};
	QueueSubmitInfo submitInfo{cmds};
	res = mDevice.queueSubmit(submitInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to submit queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_SUBMIT_FAILED);
	}

	res = mDevice.queueWaitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to wait queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_WAIT_FAILED);
	}

	res = mDevice.resetCommandPool(mCmdPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to reset commandpool for swapchain image pipelineBarrier!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_POOL_RESET_FAILED);
	}

	record();

	mCurrentPipelineIndex = mSwapchain.getPipelineLength()-1;
}

bool Application::render(double deltaSeconds)
{
	//Needed to ensure we dont run too fast as there is no enough load.
	std::this_thread::sleep_for(std::chrono::milliseconds(1));

	const size_t tempIndex = (mCurrentPipelineIndex+1) % mSwapchain.getPipelineLength();

	auto indexRes = mSwapchain.acquireNextImage(mPresentSemaphores[tempIndex]);
	if (!indexRes)
	{
		std::cerr << "Acquiring image failed!\n";
		Context::Check(indexRes.unwrapError());
		assert(false);
		std::exit(ACQUIRING_IMAGE_FAILED);
	}
	mCurrentPipelineIndex = indexRes.unwrap();

	VkResult res = mDevice.getFenceStatus(mFences[mCurrentPipelineIndex]);
	if (res == VK_SUCCESS || res == VK_NOT_READY)
	{
		res = mDevice.waitAndResetFence(mFences[mCurrentPipelineIndex]);
		if (res != VK_SUCCESS)
		{
			std::cerr << "Fence wait failed!\n";
			Context::Check(res);
			assert(false);
			std::exit(FENCE_WAIT_FAILED);
		}
	}

	// Data upload
	{
		const Image& sw{mSwapchain.getImageRef(0)};
		const glm::vec3 camPos{-2, 2, -2};
		mUBO.view = glm::lookAt(camPos, glm::vec3{}, mUp);
		mUBO.proj = glm::perspective(glm::radians(40.f), float(sw.getWidth()) / float(sw.getHeight()), 0.001f, 500.f);
		mUBO.model = glm::rotate(mUBO.model, glm::radians(float(deltaSeconds) * 5.f), mUp);

		size_t uboSize = sizeof(glm::mat4) * 3;
		size_t alignedSize = util::align(uboSize, size_t(mDevice.getBufferAlignment()));
		size_t uboOffset = alignedSize * mCurrentPipelineIndex;

		uint8_t* ubo = (uint8_t*) mUBO.buffer.map().unwrap();
		ubo += mCurrentPipelineIndex * uboOffset;
		memcpy(ubo, &mUBO, uboSize);
	}

	std::vector<CommandBuffer> cmds;
	cmds.push_back(mCmdPool.getCommandBuffer(mCurrentPipelineIndex).value());
	QueueSubmitInfo submitInfo{cmds};
	submitInfo.waitValue = mSemaphoreIndexCounters[mCurrentPipelineIndex]++;
	submitInfo.signalValue = mSemaphoreIndexCounters[mCurrentPipelineIndex];
	submitInfo.semaphore = mSemaphores[mCurrentPipelineIndex];
	submitInfo.waitFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	submitInfo.signalFence = mFences[mCurrentPipelineIndex];
	res = mDevice.queueSubmit(submitInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Pipeline queue submit failed!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_SUBMIT_FAILED);
	}

	VkSwapchainKHR sw = mSwapchain.native();
	VkPresentInfoKHR presentInfo{VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &(mPresentSemaphores[mCurrentPipelineIndex]);
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &sw;
	presentInfo.pImageIndices = &mCurrentPipelineIndex;
	res = mDevice.queuePresent(presentInfo); // VK_ERROR_OUT_OF_DATE_KHR
	if (res != VK_SUCCESS && res != VK_ERROR_OUT_OF_DATE_KHR)
	{
		std::cerr << "Presentation failed!\n";
		Context::Check(res);
		assert(false);
		std::exit(PRESENT_FAILED);
	}
	else if (res == VK_ERROR_OUT_OF_DATE_KHR)
	{
		std::cout << "Swapchain out of date!\n";
		return true;
	}
	return false;
}

// Private functions
void Application::createContext(GLFWwindow* window)
{
	auto ctx = Context::Create(window, "Triangle");
	if (!ctx)
	{
		std::cerr << "Vulkan initialization failed!\n";
		assert(false);
		std::exit(VULKAN_INIT_FAILED);
	}
	mCtx = ctx.unwrap();
}
void Application::createDevice()
{
	auto device = mCtx.createDevice();
	if (!device)
	{
		std::cerr << "Device creation failed!\n";
		Context::Check(device.unwrapError());
		assert(false);
		std::exit(DEVICE_CREATION_FAILED);
	}
	mDevice = device.unwrap();
}
void Application::loadScene()
{
	auto scene = LoadScene("res/chalet.gltf");
	if (!scene)
	{
		std::cerr << "Loading scene failed!\n";
		assert(false);
		std::exit(LOADING_SCENE_FAILED);
	}
	mSceneData = std::move(scene.value());
}
void Application::createSwapchain()
{
	auto swapchain = mDevice.createSwapchain(mCtx.getSurface());
	if (!swapchain)
	{
		std::cerr << "Failed to create swapchain!\n";
		assert(false);
		std::exit(SWAPCHAIN_CREATION_FAILED);
	}
	mSwapchain = swapchain.unwrap();
}
void Application::createUploadBuffer()
{
	VkBufferCreateInfo bufferInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
	bufferInfo.size = gPerFrameUploadBufferSize * mSwapchain.getPipelineLength();
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	VmaAllocationCreateInfo allocInfo{};
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;
	allocInfo.flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;
	allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
	allocInfo.preferredFlags = VK_MEMORY_PROPERTY_HOST_CACHED_BIT;
	auto uploadBuffer = mDevice.createBuffer(bufferInfo, allocInfo);
	if (!uploadBuffer)
	{
		std::cerr << "Upload buffer creation failed!\n";
		Context::Check(uploadBuffer.unwrapError());
		assert(false);
		std::exit(BUFFER_CREATION_FAILED);
	}
	mUploadBuffer = uploadBuffer.unwrap();
}
void Application::createShader()
{
	util::Result<Shader, VkResult> triangleShader = mDevice.createShader("res/triangle.vert", "res/triangle.frag");
	if (!triangleShader)
	{
		std::cerr << "Failed to create shader!\n";
		Context::Check(triangleShader.unwrapError());
		assert(false);
		std::exit(SHADER_CREATION_FAILED);
	}
	mTriangleShader = triangleShader.unwrap();
}
void Application::createRenderTargets()
{
	// Lets use super sampling for now
	constexpr uint32_t imgScale{2};

	mRenderTargetImages.resize(mSwapchain.getPipelineLength());
	for (Image& img : mRenderTargetImages)
	{
		const Image& swImg{mSwapchain.getImageRef(0)};

		VkImageCreateInfo imgCreate{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
		imgCreate.imageType = VK_IMAGE_TYPE_2D;
		imgCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imgCreate.format = swImg.getFormat();
		imgCreate.extent.width = swImg.getWidth() * imgScale;
		imgCreate.extent.height = swImg.getHeight() * imgScale;
		imgCreate.extent.depth = 1;
		imgCreate.arrayLayers = 1;
		imgCreate.mipLevels = 1;
		imgCreate.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		imgCreate.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imgCreate.samples = VK_SAMPLE_COUNT_1_BIT;

		VkImageViewCreateInfo viewCreate{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
		viewCreate.format = imgCreate.format;
		viewCreate.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewCreate.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewCreate.subresourceRange.baseArrayLayer = 0;
		viewCreate.subresourceRange.baseMipLevel = 0;
		viewCreate.subresourceRange.layerCount = 1;
		viewCreate.subresourceRange.levelCount = 1;

		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		allocInfo.preferredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
		auto out = mDevice.createImage(imgCreate, viewCreate, allocInfo);
		if (!out)
		{
			std::cout << "Failed to create rendertarget image!\n";
			assert(false);
			std::exit(RENDER_TARGET_CREATION_FAILED);
		}
		img = out.unwrap();
	}

	mDepthImages.resize(mSwapchain.getPipelineLength());
	for (Image& img : mDepthImages)
	{
		const Image& swImg{mSwapchain.getImageRef(0)};

		VkImageCreateInfo imgCreate{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
		imgCreate.imageType = VK_IMAGE_TYPE_2D;
		imgCreate.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imgCreate.format = gDepthFormat;
		imgCreate.extent.width = swImg.getWidth() * imgScale;
		imgCreate.extent.height = swImg.getHeight() * imgScale;
		imgCreate.extent.depth = 1;
		imgCreate.arrayLayers = 1;
		imgCreate.mipLevels = 1;
		imgCreate.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
		imgCreate.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imgCreate.samples = VK_SAMPLE_COUNT_1_BIT;

		VkImageViewCreateInfo viewCreate{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
		viewCreate.format = imgCreate.format;
		viewCreate.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewCreate.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewCreate.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		viewCreate.subresourceRange.baseArrayLayer = 0;
		viewCreate.subresourceRange.baseMipLevel = 0;
		viewCreate.subresourceRange.layerCount = 1;
		viewCreate.subresourceRange.levelCount = 1;

		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
		allocInfo.preferredFlags = VK_MEMORY_PROPERTY_LAZILY_ALLOCATED_BIT;
		auto out = mDevice.createImage(imgCreate, viewCreate, allocInfo);
		if (!out)
		{
			std::cout << "Failed to create rendertarget image!\n";
			assert(false);
			std::exit(DEPTH_IMAGE_CREATION_FAILED);
		}
		img = out.unwrap();
	}
}
void Application::createGraphicsPipeline()
{
	const Image& swImg{mSwapchain.getImageRef(0)};
	const Image& rtImg{mRenderTargetImages[0]};

	std::vector<VkAttachmentDescription> attachments{};
	{
		VkAttachmentDescription colorAttachment{};
		colorAttachment.format = swImg.getFormat();
		colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		attachments.push_back(colorAttachment);

		VkAttachmentDescription depthAttachment{};
		depthAttachment.format = gDepthFormat;
		depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
		depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		attachments.push_back(depthAttachment);
	}

	VkAttachmentReference colorAttachmentRef{};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	VkAttachmentReference depthAttachmentRef{};
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass{};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	subpass.pDepthStencilAttachment = &depthAttachmentRef;

	VkRenderPassCreateInfo renderPassInfo{VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
	renderPassInfo.attachmentCount = attachments.size();
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;

	auto renderPass = mDevice.createRenderPass(renderPassInfo);
	if (!renderPass)
	{
		Context::Check(renderPass.unwrapError());
		assert(false);
		std::exit(RENDER_PASS_CREATION_FAILED);
	}
	mRenderPass = renderPass.unwrap();

	VkViewport viewport{};
	VkRect2D scissor{};
	{
		viewport.width = (float) rtImg.getWidth();
		viewport.height = (float) rtImg.getHeight();
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		scissor.extent.width = (float) rtImg.getWidth();
		scissor.extent.height = (float) rtImg.getHeight();
	}

	std::vector<VkVertexInputAttributeDescription> inputAttributes{2};
	inputAttributes[0].offset = 0;
	inputAttributes[0].format = VK_FORMAT_R32G32B32_SFLOAT;
	inputAttributes[0].binding = 0;
	inputAttributes[0].location = 0;
	inputAttributes[1].offset = 4 * 3;
	inputAttributes[1].format = VK_FORMAT_R32G32_SFLOAT;
	inputAttributes[1].binding = 0;
	inputAttributes[1].location = 1;

	std::vector<VkVertexInputBindingDescription> inputBindings{1};
	inputBindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
	inputBindings[0].binding = 0;
	inputBindings[0].stride = (4 * 3) + (4 * 2);

	VkPipelineVertexInputStateCreateInfo vertexInputState{VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};
	vertexInputState.vertexAttributeDescriptionCount = inputAttributes.size();
	vertexInputState.pVertexAttributeDescriptions = inputAttributes.data();
	vertexInputState.vertexBindingDescriptionCount = inputBindings.size();
	vertexInputState.pVertexBindingDescriptions = inputBindings.data();

	auto pipe = mDevice.createGraphicsPipeline(mTriangleShader, viewport, scissor, mRenderPass, vertexInputState);
	if (!pipe)
	{
		Context::Check(pipe.unwrapError());
		assert(false);
		std::exit(PIPELINE_CREATION_FAILED);
	}
	mPipeline = pipe.unwrap();
}
void Application::createCommandPool()
{
	auto pool = mDevice.createCommandPool(mSwapchain.getPipelineLength());
	if (!pool)
	{
		std::cerr << "Creating CommandPool failed!\n";
		assert(false);
		std::exit(COMMAND_POOL_CREATION_FAILED);
	}
	mCmdPool = pool.unwrap();
}
void Application::convertImageDataToImages()
{
	CommandBuffer cmd{mCmdPool.getCommandBuffer(0).value()};
	VkCommandBufferBeginInfo begin{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
	VkResult res = cmd.begin(begin);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to start CommandBuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_BUFFER_BEGIN_FAILED);
	}

	mTextures.resize(mSceneData.images.size());
	size_t uploadOffset{0};
	for (size_t index = 0; index < mTextures.size(); ++index)
	{
		ImageData& source{mSceneData.images[index]};
		if (uploadOffset + util::align(source.data.size(), size_t(mDevice.getBufferAlignment())) >=
			mUploadBuffer.size())
		{
			// TODO: In future upload all data at this point
			std::cerr << "Too much stuff for uploading\n";
			assert(false);
		}

		size_t tempOffset = uploadOffset;

		void* uploadPtr = (uint8_t*) (mUploadBuffer.map().unwrap()) + uploadOffset;
		memcpy(uploadPtr, source.data.data(), source.data.size());
		uploadOffset += util::align(source.data.size(), size_t(mDevice.getBufferAlignment()));
		source.data.clear();

		VkImageCreateInfo imgInfo{VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
		imgInfo.imageType = VK_IMAGE_TYPE_2D;
		imgInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imgInfo.format = source.format;
		imgInfo.extent.width = source.width;
		imgInfo.extent.height = source.height;
		imgInfo.extent.depth = 1;
		imgInfo.arrayLayers = 1;
		imgInfo.mipLevels = 1;
		imgInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		imgInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		imgInfo.samples = VK_SAMPLE_COUNT_1_BIT;

		VkImageViewCreateInfo viewInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
		viewInfo.format = source.format;
		viewInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		viewInfo.subresourceRange.baseArrayLayer = 0;
		viewInfo.subresourceRange.baseMipLevel = 0;
		viewInfo.subresourceRange.layerCount = imgInfo.arrayLayers;
		viewInfo.subresourceRange.levelCount = imgInfo.mipLevels;

		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		allocInfo.requiredFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

		VkBufferImageCopy imageCopy{};
		imageCopy.bufferOffset = tempOffset;
		imageCopy.bufferRowLength = 0;
		imageCopy.bufferImageHeight = 0;
		imageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageCopy.imageSubresource.mipLevel = 0;
		imageCopy.imageSubresource.baseArrayLayer = 0;
		imageCopy.imageSubresource.layerCount = 1;
		imageCopy.imageOffset = {0, 0, 0};
		imageCopy.imageExtent = {static_cast<uint32_t>(source.width), static_cast<uint32_t>(source.height), 1};

		auto opt = mDevice.createImageWithStagedData(imgInfo, viewInfo, allocInfo, mUploadBuffer, imageCopy, cmd);
		if (!opt)
		{
			std::cerr << "Creating texture failed!\n";
			Context::Check(opt.unwrapError());
			assert(false);
			std::exit(TEXTURE_CREATION_FAILED);
		}
		Image texture = opt.unwrap();

		mTextures[index] = texture;
	}
	// Source images are no longer needed
	mSceneData.images.clear();

	res = cmd.end();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to end texture upload commandbuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(UPLOAD_CMD_FAILED);
	}

	std::vector<CommandBuffer> cmds{cmd};
	QueueSubmitInfo submitInfo{cmds};
	res = mDevice.queueSubmit(submitInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to submit queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_SUBMIT_FAILED);
	}

	res = mDevice.queueWaitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to wait queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_WAIT_FAILED);
	}

	// Resetting commandpool so we can just reuse cmdbuffer0 in future initializations
	res = mDevice.resetCommandPool(mCmdPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to reset commandpool for index+vertex buffers!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_POOL_RESET_FAILED);
	}
}
void Application::createChalet()
{
	ObjectData* chaletData{nullptr};
	for (ObjectData& obj : mSceneData.objects)
	{
		const std::string& name{mSceneData.names[obj.nameIndex]};
		if (name == "chalet")
		{
			chaletData = &obj;
			break;
		}
	}
	if (chaletData == nullptr)
	{
		std::cerr << "Scene doesnt seem to contain 'chalet' object!\n";
		std::cerr << "This app can only open 'chalet.gltf' converted from vulkan-tutorial.com\n";
		assert(false);
		std::exit(MODEL_MISSING);
	}
	mChalet.model = mSceneData.matrices[chaletData->transformIndex];
	const MaterialData& material{mSceneData.materials[chaletData->materials[0]]};
	mChalet.textureRef = &mTextures[material.albedoIndex];

	size_t uploadOffset{0};
	CommandBuffer cmd{mCmdPool.getCommandBuffer(0).value()};
	VkCommandBufferBeginInfo beginInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
	VkResult res = cmd.begin(beginInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to begin commandbuffer for buffer creation!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_BUFFER_BEGIN_FAILED);
	}

	const MeshData& mesh{mSceneData.meshes[chaletData->meshes[0]]};
	// Index Buffer creation and upload
	{
		const std::vector<uint32_t>& indexData{mesh.indices};

		VkBufferCreateInfo bufferInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
		bufferInfo.size = sizeof(uint32_t) * indexData.size();
		bufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		auto indexBuffer = mDevice.createBuffer(bufferInfo, allocInfo);
		if (!indexBuffer)
		{
			std::cerr << "Index buffer creation failed!\n";
			Context::Check(indexBuffer.unwrapError());
			assert(false);
			std::exit(BUFFER_CREATION_FAILED);
		}
		mChalet.indices = indexBuffer.unwrap();
		mChalet.indexCount = indexData.size();

		void* uploadPtr = (uint8_t*) (mUploadBuffer.map().unwrap()) + uploadOffset;
		memcpy(uploadPtr, indexData.data(), bufferInfo.size);

		VkBufferCopy region{};
		region.dstOffset = 0;
		region.srcOffset = uploadOffset;
		region.size = bufferInfo.size;
		mDevice.updateGPUBuffer(cmd, mUploadBuffer, mChalet.indices, region);

		uploadOffset += util::align(bufferInfo.size, size_t(mDevice.getBufferAlignment()));
	}
	// Vertex Buffer creation and upload
	{
		struct Vertex
		{
			float pos[3];
			float uv[2];
		};
		std::vector<Vertex> vertices{mesh.positions.size()};
		for (size_t index = 0; index < vertices.size(); ++index)
		{
			Vertex& out{vertices[index]};
			out.pos[0] = mesh.positions[index].x;
			out.pos[1] = mesh.positions[index].y;
			out.pos[2] = mesh.positions[index].z;
			out.uv[0] = mesh.uvs[index].x;
			out.uv[1] = mesh.uvs[index].y;
		}
		VkBufferCreateInfo bufferInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
		bufferInfo.size = sizeof(Vertex) * vertices.size();
		bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
		auto vertexBuffer = mDevice.createBuffer(bufferInfo, allocInfo);
		if (!vertexBuffer)
		{
			std::cerr << "Vertex buffer creation failed!\n";
			Context::Check(vertexBuffer.unwrapError());
			assert(false);
			std::exit(BUFFER_CREATION_FAILED);
		}
		mChalet.vertices = vertexBuffer.unwrap();

		void* uploadPtr = (uint8_t*) (mUploadBuffer.map().unwrap()) + uploadOffset;
		memcpy(uploadPtr, vertices.data(), bufferInfo.size);

		VkBufferCopy region{};
		region.dstOffset = 0;
		region.srcOffset = uploadOffset;
		region.size = bufferInfo.size;
		mDevice.updateGPUBuffer(cmd, mUploadBuffer, mChalet.vertices, region);

		uploadOffset += util::align(bufferInfo.size, size_t(mDevice.getBufferAlignment()));
	}

	res = cmd.end();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to end buffer upload commandbuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(UPLOAD_CMD_FAILED);
	}

	std::vector<CommandBuffer> cmds{cmd};
	QueueSubmitInfo submitInfo{cmds};
	res = mDevice.queueSubmit(submitInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to submit queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_SUBMIT_FAILED);
	}

	res = mDevice.queueWaitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to wait queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_WAIT_FAILED);
	}

	// Resetting commandpool so we can just reuse cmdbuffer0 in future initializations
	res = mDevice.resetCommandPool(mCmdPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to reset commandpool for recording!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_POOL_RESET_FAILED);
	}
}
void Application::createFramebuffers()
{
	const Image& rtImg{mRenderTargetImages[0]};
	const VkFormat rtFormat{rtImg.getFormat()};
	const Image& dsImg{mDepthImages[0]};
	const VkFormat dsFormat{dsImg.getFormat()};

	std::vector<VkFramebufferAttachmentImageInfo> infos{2};
	infos[0].sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO;
	infos[0].height = rtImg.getHeight();
	infos[0].width = rtImg.getWidth();
	infos[0].usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
	infos[0].layerCount = 1;
	infos[0].viewFormatCount = 1;
	infos[0].pViewFormats = &rtFormat;
	infos[1].sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENT_IMAGE_INFO;
	infos[1].height = infos[0].height;
	infos[1].width = infos[0].width;
	infos[1].usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
	infos[1].layerCount = 1;
	infos[1].viewFormatCount = 1;
	infos[1].pViewFormats = &dsFormat;

	FramebufferCreateInfo fboCreateInfo;
	fboCreateInfo.renderPass = mRenderPass;
	fboCreateInfo.layers = 1;
	fboCreateInfo.extent = {uint32_t(dsImg.getWidth()), uint32_t(dsImg.getHeight())};
	fboCreateInfo.imageAttachmentInfos = infos;
	auto fbo = mDevice.createFramebuffer(fboCreateInfo);
	if (fbo.isError())
	{
		std::cerr << "Failed to create framebuffer!\n";
		Context::Check(fbo.unwrapError());
		assert(false);
		std::exit(FRAMEBUFFER_CREATION_FAILED);
	}
	mFBO = fbo.unwrap();
}
void Application::createView()
{
	const Image& sw{mSwapchain.getImageRef(0)};

	const glm::vec3 camPos{2, 2, 2};
	mUBO.view = glm::lookAt(camPos, glm::vec3{}, mUp);
	mUBO.proj = glm::perspective(glm::radians(60.f), float(sw.getWidth()) / float(sw.getHeight()), 0.001f, 500.f);
	mUBO.model = mChalet.model;
	{
		VkBufferCreateInfo bufferInfo = {VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO};
		bufferInfo.size = util::align(sizeof(glm::mat4) * /*mvp*/ 3, size_t(mDevice.getBufferAlignment())) *
						  mSwapchain.getPipelineLength();
		bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
		allocInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT;
		allocInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
		allocInfo.preferredFlags = VK_MEMORY_PROPERTY_HOST_CACHED_BIT;

		auto buffer = mDevice.createBuffer(bufferInfo, allocInfo);
		if (!buffer)
		{
			std::cerr << "Uniform buffer creation failed!\n";
			Context::Check(buffer.unwrapError());
			assert(false);
			std::exit(BUFFER_CREATION_FAILED);
		}
		mUBO.buffer = buffer.unwrap();
	}
}
void Application::createDescriptors()
{
	// "Vertex set"
	const std::vector<VkDescriptorSetLayout>& layouts{mTriangleShader.getNativeLayouts()};
	{
		std::vector<VkDescriptorPoolSize> poolSizes;
		// Reminder: in another applications remember to multiply count
		poolSizes.emplace_back(mTriangleShader.getPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER));
		poolSizes[0].descriptorCount *= mSwapchain.getPipelineLength();

		VkDescriptorPoolCreateInfo poolCreateInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
		poolCreateInfo.maxSets = mSwapchain.getPipelineLength();
		poolCreateInfo.poolSizeCount = poolSizes.size();
		poolCreateInfo.pPoolSizes = poolSizes.data();

		auto pool = mDevice.createDescriptorPool(poolCreateInfo);
		if (!pool)
		{
			std::cerr << "DescriptorPool creation failed!\n";
			Context::Check(pool.unwrapError());
			assert(false);
			std::exit(DESCRIPTORPOOL_CREATION_FAILED);
		}
		mVertexPool = pool.unwrap();

		for (size_t index = 0; index < mSwapchain.getPipelineLength(); ++index)
		{
			VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO};
			descriptorSetAllocateInfo.descriptorPool = mVertexPool;
			descriptorSetAllocateInfo.descriptorSetCount = 1;
			descriptorSetAllocateInfo.pSetLayouts = &layouts[0];
			auto sets = mDevice.allocateDescriptorSets(descriptorSetAllocateInfo);
			if (!sets)
			{
				std::cerr << "DescriptorSet allocation failed!\n";
				Context::Check(sets.unwrapError());
				assert(false);
				std::exit(DESCRIPTOR_ALLOCATION_FAILED);
			}
			mDescSets.push_back(sets.unwrap()[0]);
		}
	}
	// "Fragment set"
	{
		std::vector<VkDescriptorPoolSize> poolSizes;
		// TODO: in another applications remember to multiply by count of render objects
		poolSizes.emplace_back(mTriangleShader.getPoolSize(VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE));
		poolSizes.emplace_back(mTriangleShader.getPoolSize(VK_DESCRIPTOR_TYPE_SAMPLER));

		VkDescriptorPoolCreateInfo poolCreateInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
		poolCreateInfo.maxSets = 1;
		poolCreateInfo.poolSizeCount = poolSizes.size();
		poolCreateInfo.pPoolSizes = poolSizes.data();

		auto pool = mDevice.createDescriptorPool(poolCreateInfo);
		if (!pool)
		{
			std::cerr << "DescriptorPool creation failed!\n";
			Context::Check(pool.unwrapError());
			assert(false);
			std::exit(DESCRIPTORPOOL_CREATION_FAILED);
		}
		mFragPool = pool.unwrap();

		VkDescriptorSetAllocateInfo descriptorSetAllocateInfo{VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO};
		descriptorSetAllocateInfo.descriptorPool = mFragPool;
		descriptorSetAllocateInfo.descriptorSetCount = 1;
		descriptorSetAllocateInfo.pSetLayouts = &layouts[1];
		auto sets = mDevice.allocateDescriptorSets(descriptorSetAllocateInfo);
		if (!sets)
		{
			std::cerr << "DescriptorSet allocation failed!\n";
			Context::Check(sets.unwrapError());
			assert(false);
			std::exit(DESCRIPTOR_ALLOCATION_FAILED);
		}
		mDescSets.push_back(sets.unwrap()[0]);
	}
}
void Application::createSampler()
{
	VkSamplerCreateInfo samplerCreateInfo{VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
	samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
	samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
	auto sampler = mDevice.createSampler(samplerCreateInfo);
	if (!sampler)
	{
		std::cerr << "Sampler creation failed!\n";
		Context::Check(sampler.unwrapError());
		assert(false);
		std::exit(SAMPLER_CREATION_FAILED);
	}
	mLinearSampler = sampler.unwrap();
}
void Application::record()
{
	// "Fragment set"
	{
		VkDescriptorSet fragSet{mDescSets[mDescSets.size() - 1]};

		VkDescriptorImageInfo samplerInfo{};
		samplerInfo.sampler = mLinearSampler;

		std::vector<VkWriteDescriptorSet> descWrites{2};
		descWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descWrites[0].dstSet = fragSet;
		descWrites[0].dstArrayElement = 0;
		descWrites[0].dstBinding = 0;
		descWrites[0].descriptorCount = 1;
		descWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
		descWrites[0].pImageInfo = &samplerInfo;

		VkDescriptorImageInfo textureInfo{};
		textureInfo.imageView = mChalet.textureRef->getView();
		textureInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		descWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descWrites[1].dstSet = fragSet;
		descWrites[1].dstArrayElement = 0;
		descWrites[1].dstBinding = 1;
		descWrites[1].descriptorCount = 1;
		descWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
		descWrites[1].pImageInfo = &textureInfo;

		mDevice.updateDescriptorSets(descWrites);
	}

	for (size_t index = 0; index < mSwapchain.getPipelineLength(); ++index)
	{
		VkDescriptorSet activeSet{mDescSets[index]};
		// "Vertex Set"
		{
			VkDescriptorBufferInfo bufferInfo{};
			bufferInfo.buffer = mUBO.buffer.native();
			bufferInfo.range = mUBO.buffer.size() / mSwapchain.getPipelineLength();
			bufferInfo.offset = bufferInfo.range * index;


			std::vector<VkWriteDescriptorSet> descWrites{1};
			descWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descWrites[0].dstSet = activeSet;
			descWrites[0].dstArrayElement = 0;
			descWrites[0].dstBinding = 0;
			descWrites[0].descriptorCount = 1;
			descWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descWrites[0].pBufferInfo = &bufferInfo;

			mDevice.updateDescriptorSets(descWrites);
		}

		CommandBuffer cmd{mCmdPool.getCommandBuffer(index).value()};
		VkCommandBufferBeginInfo beginInfo{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
		VkResult res = cmd.begin(beginInfo);
		if (res != VK_SUCCESS)
		{
			std::cerr << "CommandBuffer::begin failed!\n";
			Context::Check(res);
			assert(false);
			std::exit(COMMAND_BUFFER_BEGIN_FAILED);
		}

		// Recording Graphics task
		Image& rtImg{mRenderTargetImages[index]};
		{
			Image& dsImg{mDepthImages[index]};
			{
				std::vector<VkClearValue> clears{2};
				clears[0].color = VkClearColorValue{{0.f, 0.f, 0.f, 1.f}};
				clears[1].depthStencil = VkClearDepthStencilValue{1.f, 0};

				std::vector<VkImageView> rtAttachments{2};
				rtAttachments[0] = rtImg.getView();
				rtAttachments[1] = dsImg.getView();
				VkRenderPassAttachmentBeginInfoKHR renderPassAttachmentBeginInfo{
					VK_STRUCTURE_TYPE_RENDER_PASS_ATTACHMENT_BEGIN_INFO_KHR};
				renderPassAttachmentBeginInfo.attachmentCount = rtAttachments.size();
				renderPassAttachmentBeginInfo.pAttachments = rtAttachments.data();
				VkRenderPassBeginInfo renderPassBeginInfo{VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
				renderPassBeginInfo.pNext = &renderPassAttachmentBeginInfo;
				renderPassBeginInfo.renderPass = mRenderPass;
				renderPassBeginInfo.framebuffer = mFBO;
				renderPassBeginInfo.renderArea =
					VkRect2D{{0, 0}, {(uint32_t) rtImg.getWidth(), (uint32_t) rtImg.getHeight()}};
				renderPassBeginInfo.clearValueCount = clears.size();
				renderPassBeginInfo.pClearValues = clears.data();
				cmd.beginRenderPass(renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
			}
			cmd.bindPipeline(VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline);

			std::vector<Buffer> vertexBuffers{mChalet.vertices};
			std::vector<size_t> vertexOffsets{};
			vertexOffsets.push_back(0);
			cmd.bindVertexBuffers(0, vertexBuffers, vertexOffsets);
			cmd.bindIndexBuffer(mChalet.indices, 0, VK_INDEX_TYPE_UINT32);

			std::vector<VkDescriptorSet> sets{2};
			sets[0] = activeSet;
			sets[1] = mDescSets[mDescSets.size() - 1];
			cmd.bindDescriptorSets(VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline, 0, sets, {});

			cmd.drawIndexed(mChalet.indexCount, 1, 0, 0, 0);

			cmd.endRenderPass();
		}

		// Blitting to screen
		{
			const Image& dest{mSwapchain.getImageRef(index)};

			std::vector<VkImageMemoryBarrier> barriers{2};
			barriers[0].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barriers[0].srcAccessMask = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
			barriers[0].dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barriers[0].oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			barriers[0].newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barriers[0].srcQueueFamilyIndex = mDevice.getQueueIndex();
			barriers[0].dstQueueFamilyIndex = mDevice.getQueueIndex();
			barriers[0].image = rtImg.native();
			barriers[0].subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
			barriers[1].sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
			barriers[1].srcAccessMask = 0;
			barriers[1].dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barriers[1].oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			barriers[1].newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barriers[1].srcQueueFamilyIndex = mDevice.getQueueIndex();
			barriers[1].dstQueueFamilyIndex = mDevice.getQueueIndex();
			barriers[1].image = dest.native();
			barriers[1].subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

			// Reports problem first time even though imageInitBarriers() sets correct old layout
			cmd.pipelineBarrier(VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
								VK_DEPENDENCY_BY_REGION_BIT, {}, {}, barriers);

			VkImageBlit blitRegion{};
			blitRegion.srcSubresource.layerCount = 1;
			blitRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blitRegion.srcOffsets[1] = {int32_t(rtImg.getWidth()), int32_t(rtImg.getHeight()), 1};
			blitRegion.dstSubresource.layerCount = 1;
			blitRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
			blitRegion.dstOffsets[1] = {int32_t(dest.getWidth()), int32_t(dest.getHeight()), 1};

			cmd.blitImage(mRenderTargetImages[index], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, dest,
						  VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, {blitRegion}, VK_FILTER_LINEAR);

			barriers[0].srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
			barriers[0].dstAccessMask = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
			barriers[0].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
			barriers[0].newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
			barriers[1].srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
			barriers[1].dstAccessMask = 0;
			barriers[1].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
			barriers[1].newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			cmd.pipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
								VK_DEPENDENCY_BY_REGION_BIT, {}, {}, barriers);
		}

		res = cmd.end();
		if (res != VK_SUCCESS)
		{
			std::cerr << "CommandBuffer::end failed!\n";
			Context::Check(res);
			assert(false);
			std::exit(CMD_RECORD_FAILED);
		}
	}
}
void Application::imageInitBarriers()
{
	std::vector<VkImageMemoryBarrier> barriers{};
	barriers.reserve(mRenderTargetImages.size() + mTextures.size() + mSwapchain.getPipelineLength());

	// Render Targets
	for (Image& img : mRenderTargetImages)
	{
		VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		barrier.newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		barrier.srcQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.dstQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.image = img.native();
		barrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

		barriers.push_back(barrier);
	}

	// Swapchain
	for (size_t index = 0; index < mSwapchain.getPipelineLength(); ++index)
	{
		VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = 0;
		barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
		barrier.srcQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.dstQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.image = mSwapchain.getImageRef(index).native();
		barrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

		barriers.push_back(barrier);
	}

	// Textures
	for (Image& texture : mTextures)
	{
		VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
		barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.dstQueueFamilyIndex = mDevice.getQueueIndex();
		barrier.image = texture.native();
		barrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

		barriers.push_back(barrier);
	}

	CommandBuffer cmd{mCmdPool.getCommandBuffer(0).value()};
	VkCommandBufferBeginInfo begin{VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
	VkResult res = cmd.begin(begin);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to start CommandBuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_BUFFER_BEGIN_FAILED);
	}

	cmd.pipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_DEPENDENCY_BY_REGION_BIT,
						{}, {}, barriers);

	res = cmd.end();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to end imagebarrier commandbuffer!\n";
		Context::Check(res);
		assert(false);
		std::exit(UPLOAD_CMD_FAILED);
	}

	std::vector<CommandBuffer> cmds{cmd};
	QueueSubmitInfo submitInfo{cmds};
	res = mDevice.queueSubmit(submitInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to submit queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_SUBMIT_FAILED);
	}

	res = mDevice.queueWaitIdle();
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to wait queue!\n";
		Context::Check(res);
		assert(false);
		std::exit(QUEUE_WAIT_FAILED);
	}

	// Resetting commandpool so we can just reuse cmdbuffer0 in future initializations
	res = mDevice.resetCommandPool(mCmdPool);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Failed to reset commandpool for future use!\n";
		Context::Check(res);
		assert(false);
		std::exit(COMMAND_POOL_RESET_FAILED);
	}
}
void Application::createFencesAndSemas()
{
	mSemaphores.resize(mSwapchain.getPipelineLength());
	mPresentSemaphores.resize(mSwapchain.getPipelineLength());
	mFences.resize(mSwapchain.getPipelineLength());
	mSemaphoreIndexCounters.resize(mSwapchain.getPipelineLength());
	for (size_t index = 0; index < mSwapchain.getPipelineLength(); ++index)
	{
		auto sema = mDevice.createTimelineSemaphore(mSemaphoreIndexCounters[index]);
		if (!sema)
		{
			std::cerr << "Failed to create semaphore\n";
			Context::Check(sema.unwrapError());
			assert(false);
			std::exit(SEMAPHORE_CREATE_FAILED);
		}
		mSemaphores[index] = sema.unwrap();

		auto present = mDevice.createBinarySemaphore();
		if (!present)
		{
			std::cerr << "Failed to create semaphore\n";
			Context::Check(present.unwrapError());
			assert(false);
			std::exit(SEMAPHORE_CREATE_FAILED);
		}
		mPresentSemaphores[index] = present.unwrap();

		auto fence = mDevice.createFence();
		if (!fence)
		{
			std::cerr << "Failed to create fence\n";
			Context::Check(fence.unwrapError());
			assert(false);
			std::exit(FENCE_CREATION_FAILED);
		}
		mFences[index] = fence.unwrap();
	}
}
} // namespace renderer