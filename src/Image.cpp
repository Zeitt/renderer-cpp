#include <Device.hpp>
#include <Image.hpp>
#include <Result.hpp>

#include <iostream>

namespace renderer
{

util::Result<Image, VkResult> Device::createImage(const VkImageCreateInfo& imageCreateInfo,
												  const VkImageViewCreateInfo& vci,
												  const VmaAllocationCreateInfo& allocationCreateInfo)
{
	Image img{};
	auto res = vmaCreateImage(mAllocator, &imageCreateInfo, &allocationCreateInfo, &img.mNative, &img.mAllocation,
							  &img.mAllocInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Image creation failed!\n";
		assert(false);
		return util::Result<Image, VkResult>(res);
	}
	VkImageViewCreateInfo viewCreateInfo{vci};
	viewCreateInfo.image = img.mNative;
	res = mTable.vkCreateImageView(mDevice, &viewCreateInfo, nullptr, &img.mView);
	if (res != VK_SUCCESS)
	{
		std::cerr << "ImageView creation failed!\n";
		assert(false);
		return util::Result<Image, VkResult>(res);
	}
	img.mWidth = imageCreateInfo.extent.width;
	img.mHeight = imageCreateInfo.extent.height;
	img.mFormat = imageCreateInfo.format;
	return util::Result<Image, VkResult>(img);
}
util::Result<Image, VkResult> Device::createImageWithStagedData(const VkImageCreateInfo& imageCreateInfo,
																const VkImageViewCreateInfo& vci,
																const VmaAllocationCreateInfo& allocationCreateInfo,
																Buffer& staged, const VkBufferImageCopy& region,
																CommandBuffer& cmd)
{
	Image img{};
	auto res = vmaCreateImage(mAllocator, &imageCreateInfo, &allocationCreateInfo, &img.mNative, &img.mAllocation,
							  &img.mAllocInfo);
	if (res != VK_SUCCESS)
	{
		std::cerr << "Image creation failed!\n";
		assert(false);
		return util::Result<Image, VkResult>(res);
	}
	VkImageViewCreateInfo viewCreateInfo{vci};
	viewCreateInfo.image = img.mNative;
	res = mTable.vkCreateImageView(mDevice, &viewCreateInfo, nullptr, &img.mView);
	if (res != VK_SUCCESS)
	{
		std::cerr << "ImageView creation failed!\n";
		assert(false);
		return util::Result<Image, VkResult>(res);
	}
	VkImageMemoryBarrier barrier{VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
	barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = img.native();
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = imageCreateInfo.mipLevels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = imageCreateInfo.arrayLayers;
	barrier.srcAccessMask = 0;
	barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	cmd.pipelineBarrier(VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, {}, {}, {barrier});

	cmd.copyBufferToImage(staged, img, region);

	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	cmd.pipelineBarrier(VK_PIPELINE_STAGE_TRANSFER_BIT,
						VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT | VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, {}, {},
						{barrier});

	return util::Result<Image, VkResult>(img);
}
void Device::destroyImage(Image& img)
{
	mTable.vkDestroyImageView(mDevice, img.mView, nullptr);
	vmaDestroyImage(mAllocator, img.mNative, img.mAllocation);
}
} // namespace renderer