#include <Device.hpp>
#include <Swapchain.hpp>

#include <cassert>
#include <iostream>

namespace renderer
{
struct SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities{};
	VkSurfaceFormatKHR format{VK_FORMAT_UNDEFINED};
	VkPresentModeKHR presentMode{VK_PRESENT_MODE_FIFO_KHR};
};
util::Result<SwapChainSupportDetails, VkResult> querySwapChainSupport(VkPhysicalDevice physical, VkSurfaceKHR surface)
{
	SwapChainSupportDetails details{};
	std::vector<VkSurfaceFormatKHR> formats{};
	std::vector<VkPresentModeKHR> presentModes{};

	auto res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical, surface, &details.capabilities);
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkGetPhysicalDeviceSurfaceCapabilitiesKHR failed!\n";
		assert(false);
		return util::Result<SwapChainSupportDetails, VkResult>(res);
	}

	uint32_t count{0};
	res = vkGetPhysicalDeviceSurfaceFormatsKHR(physical, surface, &count, nullptr);
	if (res != VK_SUCCESS || count == 0)
	{
		std::cerr << "vkGetPhysicalDeviceSurfaceFormatsKHR::count failed!\n";
		assert(false);
		return util::Result<SwapChainSupportDetails, VkResult>(res);
	}
	formats.resize(count);
	res = vkGetPhysicalDeviceSurfaceFormatsKHR(physical, surface, &count, formats.data());
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkGetPhysicalDeviceSurfaceFormatsKHR failed!\n";
		assert(false);
		return util::Result<SwapChainSupportDetails, VkResult>(res);
	}

	count = 0;
	res = vkGetPhysicalDeviceSurfacePresentModesKHR(physical, surface, &count, nullptr);
	if (res != VK_SUCCESS || count == 0)
	{
		std::cerr << "vkGetPhysicalDeviceSurfacePresentModesKHR::count failed!\n";
		assert(false);
		return util::Result<SwapChainSupportDetails, VkResult>(res);
	}
	presentModes.resize(count);
	res = vkGetPhysicalDeviceSurfacePresentModesKHR(physical, surface, &count, presentModes.data());
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkGetPhysicalDeviceSurfacePresentModesKHR failed!\n";
		assert(false);
		return util::Result<SwapChainSupportDetails, VkResult>(res);
	}

	for (VkPresentModeKHR present : presentModes)
	{
		if (present == VK_PRESENT_MODE_MAILBOX_KHR)
		{
			details.presentMode = present;
			break;
		}
		else if (present == VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			details.presentMode = present;
		}
		else if (present == VK_PRESENT_MODE_FIFO_RELAXED_KHR && details.presentMode != VK_PRESENT_MODE_IMMEDIATE_KHR)
		{
			details.presentMode = present;
		}
	}

	for (VkSurfaceFormatKHR format : formats)
	{
		if (format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
		{
			details.format = format;
			if (format.format == VK_FORMAT_R8G8B8A8_SRGB || format.format == VK_FORMAT_B8G8R8A8_SRGB)
			{
				break;
			}
		}
		else
		{
			std::cout << "Currently only VK_COLOR_SPACE_SRGB_NONLINEAR_KHR is handled!\n";
		}
	}
	if (details.format.format == VK_FORMAT_UNDEFINED)
	{
		std::cerr << "Deivce doesnt support VK_COLOR_SPACE_SRGB_NONLINEAR_KHR!\n";
		return util::Result<SwapChainSupportDetails, VkResult>(VK_ERROR_FORMAT_NOT_SUPPORTED);
	}

	return util::Result<SwapChainSupportDetails, VkResult>(details);
}

util::Result<Swapchain, VkResult> Device::createSwapchain(VkSurfaceKHR surface)
{
	auto sws{querySwapChainSupport(mPhysical, surface)};
	if (!sws)
	{
		std::cerr << "Failed to query swapchain support!\n";
		assert(false);
		return util::Result<Swapchain, VkResult>(sws.unwrapError());
	}
	const SwapChainSupportDetails support{sws.unwrap()};
	Swapchain swapchain{};
	swapchain.mPipelineLength =
		std::min(support.capabilities.maxImageCount,
				 std::max((uint32_t) swapchain.mPipelineLength, support.capabilities.minImageCount));

	VkSwapchainCreateInfoKHR createInfo{VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
	createInfo.surface = surface;
	createInfo.presentMode = support.presentMode;
	createInfo.minImageCount = swapchain.mPipelineLength;
	createInfo.imageFormat = support.format.format;
	createInfo.imageColorSpace = support.format.colorSpace;
	createInfo.imageExtent = support.capabilities.currentExtent;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.preTransform = support.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.clipped = VK_FALSE;
	createInfo.imageArrayLayers = 1;

	auto res = mTable.vkCreateSwapchainKHR(mDevice, &createInfo, nullptr, &swapchain.mSwapchain);
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkCreateSwapchainKHR failed!\n";
		assert(false);
		return util::Result<Swapchain, VkResult>(res);
	}
	uint32_t swImageCount{};
	res = mTable.vkGetSwapchainImagesKHR(mDevice, swapchain.mSwapchain, &swImageCount, nullptr);
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkGetSwapchainImagesKHR::count failed!\n";
		assert(false);
		return util::Result<Swapchain, VkResult>(res);
	}
	if (swImageCount != swapchain.mPipelineLength)
	{
		std::cerr << "Invalid amount of swapchain images!\n";
		assert(false);
		return util::Result<Swapchain, VkResult>(VK_ERROR_INITIALIZATION_FAILED);
	}
	std::vector<VkImage> imgs{swImageCount};
	res = mTable.vkGetSwapchainImagesKHR(mDevice, swapchain.mSwapchain, &swImageCount, imgs.data());
	if (res != VK_SUCCESS)
	{
		std::cerr << "vkGetSwapchainImagesKHR failed!\n";
		assert(false);
		return util::Result<Swapchain, VkResult>(res);
	}

	swapchain.mImages.resize(swapchain.mPipelineLength);
	for (size_t index = 0; index < swapchain.mPipelineLength; ++index)
	{
		Image& img{swapchain.mImages[index]};
		img.mFormat = support.format.format;
		img.mNative = imgs[index];
		img.mWidth = support.capabilities.currentExtent.width;
		img.mHeight = support.capabilities.currentExtent.height;

		VkImageViewCreateInfo imgCreateInfo{VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
		imgCreateInfo.image = imgs[index];
		imgCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imgCreateInfo.format = support.format.format;
		imgCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		imgCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		imgCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		imgCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		imgCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imgCreateInfo.subresourceRange.baseMipLevel = 0;
		imgCreateInfo.subresourceRange.levelCount = 1;
		imgCreateInfo.subresourceRange.baseArrayLayer = 0;
		imgCreateInfo.subresourceRange.layerCount = 1;

		res = mTable.vkCreateImageView(mDevice, &imgCreateInfo, nullptr, &img.mView);
		if (res != VK_SUCCESS)
		{
			std::cerr << "vkCreateImageView failed!\n";
			assert(false);
			return util::Result<Swapchain, VkResult>(res);
		}
	}

	swapchain.mTable = mTable;
	swapchain.mDevice = mDevice;

	return util::Result<Swapchain, VkResult>(swapchain);
}

void Device::destroySwapchain(Swapchain& sw)
{
	mTable.vkDeviceWaitIdle(mDevice);

	for (Image& img : sw.mImages)
	{
		mTable.vkDestroyImageView(mDevice, img.mView, nullptr);
	}
	sw.mImages.clear();

	mTable.vkDestroySwapchainKHR(mDevice, sw.mSwapchain, nullptr);
}


util::Result<size_t, VkResult> Swapchain::acquireNextImage(VkSemaphore toSignal)
{
	uint32_t index{UINT32_MAX};
	VkResult res = mTable.vkAcquireNextImageKHR(mDevice, mSwapchain, UINT64_MAX, toSignal, VK_NULL_HANDLE, &index);
	if (res == VK_SUCCESS)
	{
		return util::Result<size_t, VkResult>(index);
	}
	return util::Result<size_t, VkResult>(res);
}
const Image& Swapchain::getImageRef(size_t index) const
{
	return mImages[index];
}

size_t Swapchain::getPipelineLength() const
{
	return mPipelineLength;
}
} // namespace renderer