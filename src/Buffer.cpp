#include <Buffer.hpp>
#include <Device.hpp>

#include <cassert>
#include <cstring>

namespace renderer
{
util::Result<Buffer, VkResult> Device::createBuffer(const VkBufferCreateInfo& bufferInfo,
													const VmaAllocationCreateInfo& allocCreateInfo)
{
	Buffer out{};
	auto res =
		vmaCreateBuffer(mAllocator, &bufferInfo, &allocCreateInfo, &out.mNative, &out.mAllocation, &out.mAllocInfo);
	if (res == VK_SUCCESS)
	{
		if (allocCreateInfo.flags & VMA_ALLOCATION_CREATE_MAPPED_BIT)
		{
			memset(out.mAllocInfo.pMappedData, 0, out.mAllocInfo.size);
		}
		out.mAllocator = mAllocator;
		return util::Result<Buffer, VkResult>(out);
	}
	assert(false);
	return util::Result<Buffer, VkResult>(res);
}
void Device::destroyBuffer(Buffer& buffer)
{
	vmaDestroyBuffer(mAllocator, buffer.mNative, buffer.mAllocation);
}


void Device::updateGPUBuffer(CommandBuffer& cmd, Buffer& cpuSrc, Buffer& gpuDst, const VkBufferCopy& region)
{
	mTable.vkCmdCopyBuffer(cmd.mCmdBuffer, cpuSrc.mNative, gpuDst.mNative, 1, &region);
}

util::Result<void*, VkResult> Buffer::map()
{
	if (mAllocInfo.pMappedData != nullptr)
	{
		return util::Result<void*, VkResult>(mAllocInfo.pMappedData);
	}
	else
	{
		void* ptr{nullptr};
		auto res = vmaMapMemory(mAllocator, mAllocation, &ptr);
		if (res == VK_SUCCESS)
		{
			return util::Result<void*, VkResult>(ptr);
		}
		assert(false);
		return util::Result<void*, VkResult>(res);
	}
}
VkResult Buffer::unmap()
{
	if (mAllocInfo.pMappedData == nullptr)
	{
		vmaUnmapMemory(mAllocator, mAllocation);
		return VK_SUCCESS;
	}
	else
	{
		assert(false);
		return VK_INCOMPLETE;
	}
}
} // namespace renderer