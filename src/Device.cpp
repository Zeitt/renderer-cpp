#include <Context.hpp>
#include <Device.hpp>

#include <cassert>
#include <iostream>
#include <vector>

namespace renderer
{
util::Result<Device, VkResult> Context::createDevice()
{
	Device device{};
	VkPhysicalDeviceFeatures2 physicalDeviceFeatures2{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2};
	auto res = device.getFirstPhysicalDevice(mInstance, physicalDeviceFeatures2);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Device, VkResult>(res);
	}

	VkDeviceQueueCreateInfo queueCreateInfo{VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO};
	res = device.findQueue(queueCreateInfo, mSurface);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Device, VkResult>(res);
	}
	float queuePrio = 1.f;
	queueCreateInfo.pQueuePriorities = &queuePrio;

	res = device.createLogicalDevice(queueCreateInfo, physicalDeviceFeatures2);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Device, VkResult>(res);
	}

	res = device.createMemoryAllocator(mInstance);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Device, VkResult>(res);
	}

	device.mTable.vkGetDeviceQueue(device.mDevice, queueCreateInfo.queueFamilyIndex, 0, &device.mMainQueue);
	if (device.mMainQueue == VK_NULL_HANDLE)
	{
		assert(false);
		return util::Result<Device, VkResult>(res);
	}
	device.mQueueFamilyIndex = queueCreateInfo.queueFamilyIndex;

	return util::Result<Device, VkResult>(device);
}
VkResult Device::waitIdle() const
{
	return mTable.vkDeviceWaitIdle(mDevice);
}
VkResult Device::queueSubmit(const QueueSubmitInfo& submit)
{
	std::vector<VkCommandBuffer> natives{submit.cmds.size()};
	for (size_t index = 0; index < submit.cmds.size(); ++index)
	{
		natives[index] = submit.cmds[index].mCmdBuffer;
	}
	VkTimelineSemaphoreSubmitInfo timelineSubmitInfo{VK_STRUCTURE_TYPE_TIMELINE_SEMAPHORE_SUBMIT_INFO};

	VkSubmitInfo submitInfo{VK_STRUCTURE_TYPE_SUBMIT_INFO};
	submitInfo.commandBufferCount = natives.size();
	submitInfo.pCommandBuffers = natives.data();
	if (submit.semaphore != VK_NULL_HANDLE)
	{
		timelineSubmitInfo.signalSemaphoreValueCount = 1;
		timelineSubmitInfo.waitSemaphoreValueCount = 1;
		timelineSubmitInfo.pSignalSemaphoreValues = &submit.signalValue;
		timelineSubmitInfo.pWaitSemaphoreValues = &submit.waitValue;

		submitInfo.pNext = &timelineSubmitInfo;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &submit.semaphore;
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &submit.semaphore;
		submitInfo.pWaitDstStageMask = &submit.waitFlags;
	}
	return mTable.vkQueueSubmit(mMainQueue, 1, &submitInfo, submit.signalFence);
}
VkResult Device::queuePresent(const VkPresentInfoKHR& presentInfo)
{
	return mTable.vkQueuePresentKHR(mMainQueue, &presentInfo);
}
VkResult Device::queueWaitIdle() const
{
	return mTable.vkQueueWaitIdle(mMainQueue);
}
uint32_t Device::getQueueIndex() const
{
	return mQueueFamilyIndex;
}
void Context::destroyDevice(Device& device)
{
	if (device.mDevice != VK_NULL_HANDLE)
	{
		device.mTable.vkDeviceWaitIdle(device.mDevice);
		vmaDestroyAllocator(device.mAllocator);
		device.mAllocator = VK_NULL_HANDLE;
		device.mTable.vkDestroyDevice(device.mDevice, VK_NULL_HANDLE);
		device.mDevice = VK_NULL_HANDLE;
		device.mVmaFunctions = {};
		device.mTable.~VolkDeviceTable();
	}
	device.mDevice = VK_NULL_HANDLE;
}

util::Result<Pipeline, VkResult> Device::createGraphicsPipeline(
	const Shader& shader, const VkViewport& viewport, const VkRect2D& scissor, VkRenderPass renderPass,
	const VkPipelineVertexInputStateCreateInfo& vertexInputState)
{
	Pipeline pipeline{};

	VkPipelineLayoutCreateInfo pipelineLayoutInfo{VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
	pipelineLayoutInfo.setLayoutCount = shader.mVkLayouts.size();
	pipelineLayoutInfo.pSetLayouts = shader.mVkLayouts.data();

	VkResult res = mTable.vkCreatePipelineLayout(mDevice, &pipelineLayoutInfo, nullptr, &pipeline.mLayout);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Pipeline, VkResult>(res);
	}

	VkPipelineShaderStageCreateInfo vertShaderInfo{VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO};
	vertShaderInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderInfo.module = shader.mVertex;
	vertShaderInfo.pName = "main";
	VkPipelineShaderStageCreateInfo fragShaderInfo{VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO};
	fragShaderInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderInfo.module = shader.mComputeFragment;
	fragShaderInfo.pName = "main";
	VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderInfo, fragShaderInfo};

	// TODO: move to "fill defaults" function
	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState{VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO};
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer{VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO};
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling{VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO};
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineColorBlendAttachmentState colorBlendAttachment{};
	colorBlendAttachment.colorWriteMask =
		VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_TRUE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlending{VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO};
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;

	VkPipelineDepthStencilStateCreateInfo depthStencilState{VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO};
	depthStencilState.depthTestEnable = VK_TRUE;
	depthStencilState.depthWriteEnable = VK_TRUE;
	depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencilState.depthBoundsTestEnable = VK_FALSE;
	depthStencilState.stencilTestEnable = VK_FALSE;

	VkGraphicsPipelineCreateInfo pipelineInfo{VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO};
	pipelineInfo.stageCount = 2;
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pVertexInputState = &vertexInputState;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = &depthStencilState;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = pipeline.mLayout;
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;

	res = mTable.vkCreateGraphicsPipelines(mDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline.mPipeline);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<Pipeline, VkResult>(res);
	}

	return util::Result<Pipeline, VkResult>(pipeline);
}
void Device::destroyPipeline(Pipeline& pipeline)
{
	mTable.vkDestroyPipeline(mDevice, pipeline.mPipeline, nullptr);
	mTable.vkDestroyPipelineLayout(mDevice, pipeline.mLayout, nullptr);
}

util::Result<VkRenderPass, VkResult> Device::createRenderPass(const VkRenderPassCreateInfo& renderPassInfo)
{
	VkRenderPass renderPass{VK_NULL_HANDLE};
	VkResult res = vkCreateRenderPass(mDevice, &renderPassInfo, nullptr, &renderPass);
	if (res != VK_SUCCESS)
	{
		assert(false);
		return util::Result<VkRenderPass, VkResult>(res);
	}
	return util::Result<VkRenderPass, VkResult>(renderPass);
}
void Device::destroyRenderPass(VkRenderPass renderPass)
{
	mTable.vkDestroyRenderPass(mDevice, renderPass, nullptr);
}

util::Result<VkFramebuffer, VkResult> Device::createFramebuffer(const FramebufferCreateInfo& fboCreateInfo)
{
	VkFramebufferAttachmentsCreateInfoKHR attachmentsCreateInfo{VK_STRUCTURE_TYPE_FRAMEBUFFER_ATTACHMENTS_CREATE_INFO};
	attachmentsCreateInfo.attachmentImageInfoCount = fboCreateInfo.imageAttachmentInfos.size();
	attachmentsCreateInfo.pAttachmentImageInfos = fboCreateInfo.imageAttachmentInfos.data();

	VkFramebufferCreateInfo realCreateInfo{VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
	realCreateInfo.pNext = &attachmentsCreateInfo, realCreateInfo.layers = fboCreateInfo.layers;
	realCreateInfo.renderPass = fboCreateInfo.renderPass;
	realCreateInfo.width = fboCreateInfo.extent.width;
	realCreateInfo.height = fboCreateInfo.extent.height;
	realCreateInfo.flags = VK_FRAMEBUFFER_CREATE_IMAGELESS_BIT_KHR;
	realCreateInfo.attachmentCount = attachmentsCreateInfo.attachmentImageInfoCount;

	VkFramebuffer fbo{VK_NULL_HANDLE};
	VkResult res = mTable.vkCreateFramebuffer(mDevice, &realCreateInfo, nullptr, &fbo);
	if (res != VK_SUCCESS)
	{
		return util::Result<VkFramebuffer, VkResult>(res);
	}
	return util::Result<VkFramebuffer, VkResult>(fbo);
}
void Device::destroyFramebuffer(VkFramebuffer fbo)
{
	mTable.vkDestroyFramebuffer(mDevice, fbo, nullptr);
}

util::Result<VkDescriptorPool, VkResult> Device::createDescriptorPool(const VkDescriptorPoolCreateInfo& info)
{
	VkDescriptorPool pool{VK_NULL_HANDLE};
	auto ret = mTable.vkCreateDescriptorPool(mDevice, &info, nullptr, &pool);
	if (ret != VK_SUCCESS)
	{
		return util::Result<VkDescriptorPool, VkResult>(ret);
	}
	return util::Result<VkDescriptorPool, VkResult>(pool);
}
void Device::destroyDescriptorPool(VkDescriptorPool pool)
{
	mTable.vkDestroyDescriptorPool(mDevice, pool, nullptr);
}

util::Result<std::vector<VkDescriptorSet>, VkResult> Device::allocateDescriptorSets(
	const VkDescriptorSetAllocateInfo& info)
{
	std::vector<VkDescriptorSet> sets{info.descriptorSetCount};
	auto ret = mTable.vkAllocateDescriptorSets(mDevice, &info, sets.data());
	if (ret != VK_SUCCESS)
	{
		return util::Result<std::vector<VkDescriptorSet>, VkResult>(ret);
	}
	return util::Result<std::vector<VkDescriptorSet>, VkResult>(sets);
}
void Device::updateDescriptorSets(const std::vector<VkWriteDescriptorSet>& writes)
{
	mTable.vkUpdateDescriptorSets(mDevice, writes.size(), writes.data(), 0, nullptr);
}
void Device::freeDescriptorSets(VkDescriptorPool pool, std::vector<VkDescriptorSet>& sets)
{
	mTable.vkFreeDescriptorSets(mDevice, pool, sets.size(), sets.data());
	sets.clear();
}

util::Result<VkSampler, VkResult> Device::createSampler(const VkSamplerCreateInfo& info)
{
	VkSampler sampler{VK_NULL_HANDLE};
	VkResult res = mTable.vkCreateSampler(mDevice, &info, nullptr, &sampler);
	if (res != VK_SUCCESS)
	{
		return util::Result<VkSampler, VkResult>(res);
	}
	return util::Result<VkSampler, VkResult>(sampler);
}
void Device::destroySampler(VkSampler sampler)
{
	mTable.vkDestroySampler(mDevice, sampler, nullptr);
}

util::Result<VkSemaphore, VkResult> Device::createTimelineSemaphore(size_t initialValue)
{
	VkSemaphoreTypeCreateInfo timelineCreateInfo{VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO};
	timelineCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
	timelineCreateInfo.initialValue = initialValue;

	VkSemaphoreCreateInfo createInfo{VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
	createInfo.pNext = &timelineCreateInfo;
	createInfo.flags = 0;

	VkSemaphore semaphore{nullptr};
	VkResult res = mTable.vkCreateSemaphore(mDevice, &createInfo, nullptr, &semaphore);
	if (res != VK_SUCCESS)
	{
		return util::Result<VkSemaphore, VkResult>(res);
	}
	return util::Result<VkSemaphore, VkResult>(semaphore);
}
util::Result<VkSemaphore, VkResult> Device::createBinarySemaphore()
{
	VkSemaphoreCreateInfo createInfo{VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
	VkSemaphore semaphore{nullptr};
	VkResult res = mTable.vkCreateSemaphore(mDevice, &createInfo, nullptr, &semaphore);
	if (res != VK_SUCCESS)
	{
		return util::Result<VkSemaphore, VkResult>(res);
	}
	return util::Result<VkSemaphore, VkResult>(semaphore);
}
VkResult Device::signalTimelineSemaphore(VkSemaphore sema, size_t value)
{
	VkSemaphoreSignalInfoKHR info{VK_STRUCTURE_TYPE_SEMAPHORE_SIGNAL_INFO_KHR};
	info.semaphore = sema;
	info.value = value;
	return mTable.vkSignalSemaphoreKHR(mDevice, &info);
}
VkResult Device::waitTimelineSemaphore(VkSemaphore sema, size_t val, uint64_t timeout /*= 1'000'000'000*/)
{
	uint64_t value = val;
	VkSemaphoreWaitInfo info{VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO_KHR};
	info.semaphoreCount = 1;
	info.pSemaphores = &sema;
	info.pValues = &value;
	return mTable.vkWaitSemaphoresKHR(mDevice, &info, timeout);
}
void Device::destroySemaphore(VkSemaphore sema)
{
	mTable.vkDestroySemaphore(mDevice, sema, nullptr);
}

util::Result<VkFence, VkResult> Device::createFence()
{
	VkFence fence{VK_NULL_HANDLE};
	VkFenceCreateInfo createInfo{VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
	createInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
	VkResult res = mTable.vkCreateFence(mDevice, &createInfo, nullptr, &fence);
	if (res != VK_SUCCESS)
	{
		return util::Result<VkFence, VkResult>(res);
	}
	return util::Result<VkFence, VkResult>(fence);
}
VkResult Device::waitAndResetFence(VkFence fence, bool reset /*= true*/)
{
	VkResult res = mTable.vkWaitForFences(mDevice, 1, &fence, VK_TRUE, 1'000'000'000);
	if (res == VK_SUCCESS)
	{
		res = mTable.vkResetFences(mDevice, 1, &fence);
	}
	return res;
}
VkResult Device::getFenceStatus(VkFence fence)
{
	return mTable.vkGetFenceStatus(mDevice, fence);
}
void Device::destroyFence(VkFence fence)
{
	mTable.vkDestroyFence(mDevice, fence, nullptr);
}

// Private functions
VkResult Device::getFirstPhysicalDevice(VkInstance instance, VkPhysicalDeviceFeatures2& features2)
{
	uint32_t physicalCount{0};
	auto res = vkEnumeratePhysicalDevices(instance, &physicalCount, nullptr);
	if (physicalCount == 0 || res != VK_SUCCESS)
	{
		std::cerr << "Failed to find Vulkan capable GPU!\n";
		return res;
	}
	physicalCount = 1;
	res = vkEnumeratePhysicalDevices(instance, &physicalCount, &(mPhysical));
	if (res != VK_SUCCESS)
	{
		assert(false);
		return res;
	}
	VkPhysicalDeviceProperties2 physicalDeviceProperties2{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2};
	vkGetPhysicalDeviceProperties2(mPhysical, &physicalDeviceProperties2);
	std::cout << "Selected first device: '" << physicalDeviceProperties2.properties.deviceName << "'\n";
	vkGetPhysicalDeviceFeatures2(mPhysical, &features2);

	mBufferAlignment = std::max(physicalDeviceProperties2.properties.limits.minStorageBufferOffsetAlignment,
								physicalDeviceProperties2.properties.limits.minUniformBufferOffsetAlignment);
	mBufferAlignment = std::max(mBufferAlignment, physicalDeviceProperties2.properties.limits.minMemoryMapAlignment);
	mBufferAlignment =
		std::max(mBufferAlignment, physicalDeviceProperties2.properties.limits.minTexelBufferOffsetAlignment);

	return VK_SUCCESS;
}
VkResult Device::createLogicalDevice(const VkDeviceQueueCreateInfo& queueCreateInfo,
									 const VkPhysicalDeviceFeatures2& physicalDeviceFeatures2)
{
	const std::vector<const char*> extensions{
		VK_KHR_SWAPCHAIN_EXTENSION_NAME,		  VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME,
		VK_KHR_TIMELINE_SEMAPHORE_EXTENSION_NAME, VK_KHR_IMAGELESS_FRAMEBUFFER_EXTENSION_NAME,
		VK_KHR_IMAGE_FORMAT_LIST_EXTENSION_NAME,
	};
	VkPhysicalDeviceTimelineSemaphoreFeaturesKHR semaphoreFeatures{
		VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_TIMELINE_SEMAPHORE_FEATURES_KHR};
	semaphoreFeatures.timelineSemaphore = VK_TRUE;
	VkPhysicalDeviceImagelessFramebufferFeaturesKHR imglessFeatures{
		VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGELESS_FRAMEBUFFER_FEATURES_KHR};
	imglessFeatures.imagelessFramebuffer = VK_TRUE;
	imglessFeatures.pNext = &semaphoreFeatures;
	VkPhysicalDeviceFeatures2 enabledDeviceFeatures2{VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2};
	enabledDeviceFeatures2.pNext = &imglessFeatures;
	enabledDeviceFeatures2.features.textureCompressionASTC_LDR =
		physicalDeviceFeatures2.features.textureCompressionASTC_LDR;
	enabledDeviceFeatures2.features.textureCompressionBC = physicalDeviceFeatures2.features.textureCompressionBC;
	enabledDeviceFeatures2.features.samplerAnisotropy = physicalDeviceFeatures2.features.samplerAnisotropy;
	enabledDeviceFeatures2.features.multiDrawIndirect = physicalDeviceFeatures2.features.multiDrawIndirect;
	VkDeviceCreateInfo mDeviceCreateInfo{VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
	mDeviceCreateInfo.pEnabledFeatures = nullptr;
	mDeviceCreateInfo.pNext = &enabledDeviceFeatures2;
	mDeviceCreateInfo.queueCreateInfoCount = 1;
	mDeviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
	mDeviceCreateInfo.enabledExtensionCount = extensions.size();
	mDeviceCreateInfo.ppEnabledExtensionNames = extensions.data();
	return vkCreateDevice(mPhysical, &mDeviceCreateInfo, nullptr, &mDevice);
}
VkResult Device::createMemoryAllocator(VkInstance instance)
{
	volkLoadDeviceTable(&mTable, mDevice);
	mVmaFunctions.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
	mVmaFunctions.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
	mVmaFunctions.vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2;
	mVmaFunctions.vkAllocateMemory = mTable.vkAllocateMemory;
	mVmaFunctions.vkFreeMemory = mTable.vkFreeMemory;
	mVmaFunctions.vkMapMemory = mTable.vkMapMemory;
	mVmaFunctions.vkUnmapMemory = mTable.vkUnmapMemory;
	mVmaFunctions.vkFlushMappedMemoryRanges = mTable.vkFlushMappedMemoryRanges;
	mVmaFunctions.vkInvalidateMappedMemoryRanges = mTable.vkInvalidateMappedMemoryRanges;
	mVmaFunctions.vkBindBufferMemory = mTable.vkBindBufferMemory;
	mVmaFunctions.vkBindImageMemory = mTable.vkBindImageMemory;
	mVmaFunctions.vkGetBufferMemoryRequirements = mTable.vkGetBufferMemoryRequirements,
	mVmaFunctions.vkGetImageMemoryRequirements = mTable.vkGetImageMemoryRequirements,
	mVmaFunctions.vkCreateBuffer = mTable.vkCreateBuffer;
	mVmaFunctions.vkDestroyBuffer = mTable.vkDestroyBuffer;
	mVmaFunctions.vkCreateImage = mTable.vkCreateImage;
	mVmaFunctions.vkDestroyImage = mTable.vkDestroyImage;
	mVmaFunctions.vkCmdCopyBuffer = mTable.vkCmdCopyBuffer;
	mVmaFunctions.vkGetBufferMemoryRequirements2KHR = mTable.vkGetBufferMemoryRequirements2;
	mVmaFunctions.vkGetImageMemoryRequirements2KHR = mTable.vkGetImageMemoryRequirements2;
	mVmaFunctions.vkBindBufferMemory2KHR = mTable.vkBindBufferMemory2;
	mVmaFunctions.vkBindImageMemory2KHR = mTable.vkBindImageMemory2;

	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = mPhysical;
	allocatorInfo.device = mDevice;
	allocatorInfo.instance = instance;
	allocatorInfo.flags =
		VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT | VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT;
	allocatorInfo.pVulkanFunctions = &mVmaFunctions;
	allocatorInfo.vulkanApiVersion = VK_API_VERSION_1_1;
	return vmaCreateAllocator(&allocatorInfo, &mAllocator);
}
VkResult Device::findQueue(VkDeviceQueueCreateInfo& queueCreateInfo, VkSurfaceKHR surface)
{
	queueCreateInfo.queueCount = 1;

	std::vector<VkQueueFamilyProperties2> properties;
	uint32_t count{0};
	vkGetPhysicalDeviceQueueFamilyProperties2(mPhysical, &count, nullptr);
	properties.resize(count);
	for (auto&& prop : properties)
	{
		prop.sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
	}
	vkGetPhysicalDeviceQueueFamilyProperties2(mPhysical, &count, properties.data());
	bool queueFound{false};
	for (uint32_t index = 0; index < count; ++index)
	{
		const VkQueueFamilyProperties2& prop{properties[index]};
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(mPhysical, index, surface, &presentSupport);
		if (presentSupport == VK_TRUE &&
			prop.queueFamilyProperties.queueFlags & VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT &&
			prop.queueFamilyProperties.queueFlags & VkQueueFlagBits::VK_QUEUE_COMPUTE_BIT &&
			prop.queueFamilyProperties.queueFlags & VkQueueFlagBits::VK_QUEUE_TRANSFER_BIT)
		{
			queueFound = true;
			queueCreateInfo.queueFamilyIndex = index;
			break;
		}
	}

	return queueFound ? VK_SUCCESS : VK_ERROR_UNKNOWN;
}
} // namespace renderer