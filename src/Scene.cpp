#include <Scene.hpp>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

#include <stb/stb_image.h>

#include <cassert>
#include <functional>
#include <iostream>

namespace renderer
{
void recursive(aiNode* root, const std::function<void(aiNode*)>& func)
{
	func(root);
	for (size_t st = 0; st < root->mNumChildren; ++st)
	{
		recursive(root->mChildren[st], func);
	}
}
glm::mat4 matrixConversion(const aiMatrix4x4 from)
{
	glm::mat4 to;
	to[0][0] = from.a1;
	to[1][0] = from.a2;
	to[2][0] = from.a3;
	to[3][0] = from.a4;
	to[0][1] = from.b1;
	to[1][1] = from.b2;
	to[2][1] = from.b3;
	to[3][1] = from.b4;
	to[0][2] = from.c1;
	to[1][2] = from.c2;
	to[2][2] = from.c3;
	to[3][2] = from.c4;
	to[0][3] = from.d1;
	to[1][3] = from.d2;
	to[2][3] = from.d3;
	to[3][3] = from.d4;
	return to;
}

std::optional<SceneData> LoadScene(const char* path)
{
	SceneData temp{};
	Assimp::Importer importer;
	const aiScene* scene =
		importer.ReadFile(path, aiProcessPreset_TargetRealtime_Quality | aiProcess_OptimizeMeshes |
									aiProcess_FindInstances | aiProcess_CalcTangentSpace | aiProcess_Triangulate);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cerr << "ERROR: assimp failed to open file: '" << importer.GetErrorString() << "'\n";
		return std::nullopt;
	}

	temp.materials.resize(scene->mNumMaterials);
	temp.images.reserve(temp.materials.size() * 3 /*albedo, normal, metalnessRoughness*/);
	for (size_t index = 0; index < temp.materials.size(); ++index)
	{
		MaterialData& material{temp.materials[index]};
		aiMaterial* in = scene->mMaterials[index];
		auto inName = in->GetName();
		if (inName.length > 0)
		{
			material.nameIndex = temp.names.size();
			temp.names.emplace_back(inName.C_Str());
		}

		if (in->GetTextureCount(aiTextureType_DIFFUSE) > 1)
		{
			ImageData img{};
			aiString texturePath{};
			in->GetTexture(aiTextureType_DIFFUSE, 1, &texturePath);

			std::string filename = path;
			if (filename.rfind('/') != std::string::npos)
			{
				filename = filename.substr(0, filename.rfind('/') + 1);
			}
			filename += texturePath.C_Str();

			int w{}, h{}, c{0};
			stbi_uc* data = stbi_load(filename.c_str(), &w, &h, &c, 0);
			if (c == 3)
			{
				stbi_image_free(data);
				data = stbi_load(filename.c_str(), &w, &h, &c, 4);
				c = 4;
			}
			img.width = w;
			img.height = h;
			img.data.resize(w * h * c);
			memcpy(img.data.data(), data, w * h * c);
			stbi_image_free(data);

			switch (c)
			{
			case 1:
				img.format = VK_FORMAT_R8_SRGB;
				break;
			case 2:
				img.format = VK_FORMAT_R8G8_SRGB;
				break;
			case 4:
				img.format = VK_FORMAT_R8G8B8A8_SRGB;
				break;
			default:
				assert(false);
				return std::nullopt;
			}

			material.albedoIndex = temp.images.size();
			temp.images.push_back(img);

			img.nameIndex = temp.names.size();
			temp.names.push_back(filename);
		}
		if (in->GetTextureCount(aiTextureType_NORMALS) > 0)
		{
			ImageData img{};
			aiString texturePath{};
			in->GetTexture(aiTextureType_NORMALS, 0, &texturePath);

			std::string filename = path;
			if (filename.rfind('/') != std::string::npos)
			{
				filename = filename.substr(0, filename.rfind('/') + 1);
			}
			filename += texturePath.C_Str();

			int w{}, h{}, c{0};
			stbi_uc* data = stbi_load(filename.c_str(), &w, &h, &c, 0);
			if (c == 3)
			{
				stbi_image_free(data);
				data = stbi_load(filename.c_str(), &w, &h, &c, 4);
				c = 4;
			}
			img.width = w;
			img.height = h;
			img.data.resize(w * h * c);
			memcpy(img.data.data(), data, w * h * c);
			stbi_image_free(data);

			switch (c)
			{
			case 1:
				img.format = VK_FORMAT_R8_UNORM;
				break;
			case 2:
				img.format = VK_FORMAT_R8G8_UNORM;
				break;
			case 4:
				img.format = VK_FORMAT_R8G8B8A8_UNORM;
				break;
			default:
				assert(false);
				return std::nullopt;
			}

			material.normalIndex = temp.images.size();
			temp.images.push_back(img);

			img.nameIndex = temp.names.size();
			temp.names.push_back(filename);
		}
		if (in->GetTextureCount(aiTextureType_UNKNOWN) > 0)
		{
			ImageData img{};
			aiString texturePath{};
			in->GetTexture(aiTextureType_UNKNOWN, 0, &texturePath);

			std::string filename = path;
			if (filename.rfind('/') != std::string::npos)
			{
				filename = filename.substr(0, filename.rfind('/') + 1);
			}
			filename += texturePath.C_Str();

			int w{}, h{}, c{0};
			stbi_uc* data = stbi_load(filename.c_str(), &w, &h, &c, 0);
			if (c == 3)
			{
				stbi_image_free(data);
				data = stbi_load(filename.c_str(), &w, &h, &c, 4);
				c = 4;
			}
			img.width = w;
			img.height = h;
			img.data.resize(w * h * c);
			memcpy(img.data.data(), data, w * h * c);
			stbi_image_free(data);

			switch (c)
			{
			case 1:
				img.format = VK_FORMAT_R8_UNORM;
				break;
			case 2:
				img.format = VK_FORMAT_R8G8_UNORM;
				break;
			case 4:
				img.format = VK_FORMAT_R8G8B8A8_UNORM;
				break;
			default:
				assert(false);
				return std::nullopt;
			}

			material.metalnessRoughnessIndex = temp.images.size();
			temp.images.push_back(img);

			img.nameIndex = temp.names.size();
			temp.names.push_back(filename);
		}
	}

	temp.meshes.resize(scene->mNumMeshes);
	for (size_t index = 0; index < temp.meshes.size(); ++index)
	{
		MeshData& mesh{temp.meshes[index]};
		aiMesh* in = scene->mMeshes[index];
		mesh.indices.resize(in->mNumFaces * 3);
		for (size_t faceIndex = 0; faceIndex < in->mNumFaces; ++faceIndex)
		{
			aiFace face = in->mFaces[faceIndex];
			if (face.mNumIndices == 3)
			{
				const size_t outIndex = faceIndex * 3;
				mesh.indices[outIndex + 0] = face.mIndices[0];
				mesh.indices[outIndex + 1] = face.mIndices[1];
				mesh.indices[outIndex + 2] = face.mIndices[2];
			}
		}
		mesh.positions.resize(in->mNumVertices);
		mesh.normals.resize(in->mNumVertices);
		mesh.uvs.resize(in->mNumVertices);
		mesh.tangents.resize(in->mNumVertices);
		for (size_t vertexIndex = 0; vertexIndex < mesh.positions.size(); ++vertexIndex)
		{
			glm::vec3& posOut{mesh.positions[vertexIndex]};
			aiVector3D posIn = in->mVertices[vertexIndex];
			posOut.x = posIn.x;
			posOut.y = posIn.y;
			posOut.z = posIn.z;

			glm::vec3& normalOut{mesh.normals[vertexIndex]};
			aiVector3D normalIn = in->mNormals[vertexIndex];
			normalOut.x = normalIn.x;
			normalOut.y = normalIn.y;
			normalOut.z = normalIn.z;

			glm::vec2& uvOut{mesh.uvs[vertexIndex]};
			aiVector3D uvIn = in->mTextureCoords[0][vertexIndex];
			uvOut.x = uvIn.x;
			uvOut.y = 1.f - uvIn.y;

			glm::vec3& tanOut{mesh.tangents[vertexIndex]};
			aiVector3D tanIn = in->mTangents[vertexIndex];
			tanOut.x = tanIn.x;
			tanOut.y = tanIn.y;
			tanOut.z = tanIn.z;
		}
	}

	size_t lastSetSize = 10;
	temp.objects.reserve(lastSetSize);
	temp.matrices.reserve(lastSetSize);
	size_t nodeIndex{0};
	recursive(scene->mRootNode, [&temp, &nodeIndex, &lastSetSize, scene](aiNode* node) {
		if (temp.objects.size() >= lastSetSize)
		{
			lastSetSize =
				(lastSetSize >= 1000) ? (lastSetSize + 1000) : ((lastSetSize >= 100) ? (lastSetSize + 100) : 10);
			temp.objects.reserve(lastSetSize);
			temp.matrices.reserve(lastSetSize);
		}
		temp.objects.emplace_back(/*Empty node*/);
		temp.matrices.emplace_back(matrixConversion(node->mTransformation));
		temp.objects[nodeIndex].children.resize(node->mNumChildren);
		for (size_t index = 0; index < node->mNumChildren; ++index)
		{
			temp.objects[nodeIndex].children[index] = nodeIndex + (index + 1);
		}
		ObjectData& obj{temp.objects[nodeIndex]};
		if (node->mName.length > 0)
		{
			obj.nameIndex = temp.names.size();
			temp.names.emplace_back(node->mName.C_Str());
		}
		obj.meshes.resize(node->mNumMeshes);
		obj.materials.resize(node->mNumMeshes);
		for (size_t index = 0; index < node->mNumMeshes; ++index)
		{
			size_t meshIndex = node->mMeshes[index];
			obj.meshes[index] = meshIndex;
			aiMesh* mesh = scene->mMeshes[meshIndex];
			obj.materials[index] = mesh->mMaterialIndex;
		}

		obj.transformIndex = nodeIndex;
		nodeIndex++;
	});

	return std::optional(temp);
}
} // namespace renderer
