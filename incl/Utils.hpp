#ifndef RENDERER_UTILS_HPP
#define RENDERER_UTILS_HPP

#include <cstdint>
#include <limits>

namespace util
{
template <typename T>
[[nodiscard]] T align(T what, T to)
{
	static_assert(std::numeric_limits<T>::is_integer);
	return what + (to - (what % to));
}
} // namespace util

#endif // RENDERER_UTILS_HPP
