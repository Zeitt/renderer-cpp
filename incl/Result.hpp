#ifndef RENDERER_RESULT_HPP
#define RENDERER_RESULT_HPP

#include <cassert>
#include <utility>

namespace util
{
template <typename ok_t, typename error_t>
class Result
{
public:
	explicit inline Result(const ok_t& ok) : mOk(ok), mIsOK(true)
	{
	}
	explicit inline Result(const ok_t&& ok) : mOk(std::move(ok)), mIsOK(true)
	{
	}
	explicit inline Result(const error_t& ok) : mError(ok), mIsOK(false)
	{
	}
	explicit inline Result(const error_t&& ok) : mError(std::move(ok)), mIsOK(false)
	{
	}
	~Result()
	{
		if (isOk())
		{
			mOk.~ok_t();
		}
		else
		{
			mError.~error_t();
		}
	};

	[[nodiscard]] inline bool isError() const
	{
		return !mIsOK;
	}
	[[nodiscard]] inline bool isOk() const
	{
		return mIsOK;
	}

	[[nodiscard]] inline error_t unwrapError()
	{
		assert(isError());
		return std::move(mError);
	}

	[[nodiscard]] inline ok_t unwrap()
	{
		assert(isOk());
		return std::move(mOk);
	}
	[[nodiscard]] inline ok_t unwrapOr(ok_t&& other)
	{
		if (isOk())
			return std::move(mOk);
		else
			return std::move(other);
	}

	explicit inline operator bool()
	{
		return isOk();
	}

private:
	union {
		ok_t mOk{};
		error_t mError;
	};
	bool mIsOK{false};
};
} // namespace util

#endif // RENDERER_RESULT_HPP
