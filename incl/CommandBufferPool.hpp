#ifndef RENDERER_COMMANDBUFFERPOOL_HPP
#define RENDERER_COMMANDBUFFERPOOL_HPP

#include <Buffer.hpp>
#include <Image.hpp>

#include <volk.h>

#include <optional>
#include <vector>

namespace renderer
{
class Pipeline
{
private:
	VkPipelineLayout mLayout{VK_NULL_HANDLE};
	VkPipeline mPipeline{VK_NULL_HANDLE};

	friend class Device;
	friend class CommandBuffer;
};

class CommandBuffer
{
public:
	CommandBuffer(const CommandBuffer&);

	[[nodiscard]] VkResult begin(const VkCommandBufferBeginInfo& info);
	[[nodiscard]] VkResult end();

	void pipelineBarrier(VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask,
						 VkDependencyFlags dependencyFlags, const std::vector<VkMemoryBarrier>&,
						 const std::vector<VkBufferMemoryBarrier>&, const std::vector<VkImageMemoryBarrier>&);

	void copyBufferToImage(Buffer& buffer, Image& image, const VkBufferImageCopy& region);
	void blitImage(const Image& src, VkImageLayout srcLayout, const Image& dst, VkImageLayout dstLayout,
				   const std::vector<VkImageBlit>&, VkFilter);

	void beginRenderPass(const VkRenderPassBeginInfo&, VkSubpassContents);
	void bindPipeline(VkPipelineBindPoint, const Pipeline&);
	void bindVertexBuffers(size_t firstBindPoint, const std::vector<Buffer>& buffers,
						   const std::vector<size_t>& offsets);
	void bindIndexBuffer(const Buffer& buffer, size_t offset, VkIndexType type);
	void bindDescriptorSets(VkPipelineBindPoint, const Pipeline&, size_t firstSet, const std::vector<VkDescriptorSet>&,
							const std::vector<uint32_t>& dynamicOffsets);
	void drawIndexed(uint32_t indexCount, uint32_t instanceCount, uint32_t firstIndex, int32_t vertexOffset,
					 uint32_t firstInstance);
	void endRenderPass();

private:
	explicit CommandBuffer(VolkDeviceTable& table, VkCommandBuffer cmd);

	VolkDeviceTable& mTable;
	VkCommandBuffer mCmdBuffer{VK_NULL_HANDLE};

	friend class Device;
};

class CommandPool
{
public:
	[[nodiscard]] std::optional<CommandBuffer> getCommandBuffer(size_t index);

private:
	VkCommandPool mPool{VK_NULL_HANDLE};
	std::vector<CommandBuffer> mBuffers;

	friend class Device;
};
} // namespace renderer

#endif // RENDERER_COMMANDBUFFERPOOL_HPP
