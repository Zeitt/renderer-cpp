#ifndef RENDERER_CONTEXT_HPP
#define RENDERER_CONTEXT_HPP

#include <Device.hpp>
#include <Result.hpp>
struct GLFWwindow;

namespace renderer
{
class Context
{
public:
	[[nodiscard]] static util::Result<Context, VkResult> Create(GLFWwindow*, const char* applicationName);

	void destroy();

	[[nodiscard]] util::Result<Device, VkResult> createDevice();
	void destroyDevice(Device& device);

	static void Check(VkResult result);

	[[nodiscard]] inline VkSurfaceKHR getSurface()
	{
		return mSurface;
	}

private:
	VkInstance mInstance{VK_NULL_HANDLE};
	VkSurfaceKHR mSurface{VK_NULL_HANDLE};
	VkDebugUtilsMessengerEXT mMessenger{VK_NULL_HANDLE};

	[[nodiscard]] VkResult initializeInstance(const char* applicationName);
};
} // namespace renderer

#endif // RENDERER_CONTEXT_HPP
