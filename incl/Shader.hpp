#ifndef RENDERER_SHADER_HPP
#define RENDERER_SHADER_HPP

#include <volk.h>

#include <cstdint>
#include <string>
#include <vector>

namespace renderer
{
class Shader
{
public:
	constexpr static uint32_t TECHNIQUE_SET{0};
	constexpr static uint32_t OBJECT_SET{1};
	constexpr static uint32_t MAX_SETS{2};

	using Stage_t = uint32_t;
	enum : Stage_t
	{
		VERTEX_STAGE = 1,
		FRAGMENT_STAGE = 2,
		COMPUTE_STAGE = 4,
	};

	struct Binding
	{
		VkDescriptorType type{VK_DESCRIPTOR_TYPE_MAX_ENUM};
		Stage_t stages{0};
	};
	using DescriptorSetLayout = std::vector<Binding>;

	[[nodiscard]] const std::vector<VkDescriptorSetLayout>& getNativeLayouts() const;
	[[nodiscard]] VkDescriptorPoolSize getPoolSize(VkDescriptorType) const;

private:
	std::vector<DescriptorSetLayout> mLayouts{MAX_SETS};
	std::vector<VkDescriptorSetLayout> mVkLayouts{};

	VkShaderModule mComputeFragment{VK_NULL_HANDLE};
	VkShaderModule mVertex{VK_NULL_HANDLE};

	friend class Device;
};
} // namespace renderer

#endif // RENDERER_SHADER_HPP
