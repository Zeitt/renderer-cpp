#ifndef RENDERER_BUFFER_HPP
#define RENDERER_BUFFER_HPP

#include <Result.hpp>

#include <vk_mem_alloc.h>
#include <volk.h>

namespace renderer
{
class Buffer
{
public:
	[[nodiscard]] util::Result<void*, VkResult> map();
	[[nodiscard]] VkResult unmap();

	[[nodiscard]] size_t size() const
	{
		return mAllocInfo.size;
	}
	[[nodiscard]] VkBuffer native() const
	{
		return mNative;
	}

private:
	VkBuffer mNative{VK_NULL_HANDLE};
	VmaAllocation mAllocation{VK_NULL_HANDLE};
	VmaAllocationInfo mAllocInfo{};
	VmaAllocator mAllocator{};

	friend class Device;
};
} // namespace renderer

#endif // RENDERER_BUFFER_HPP
