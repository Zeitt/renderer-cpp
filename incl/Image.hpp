#ifndef RENDERER_IMAGE_HPP
#define RENDERER_IMAGE_HPP

#include <vk_mem_alloc.h>
#include <volk.h>

namespace renderer
{
class Image
{
public:
	[[nodiscard]] VkImage native() const
	{
		return mNative;
	}
	[[nodiscard]] VkImageView getView() const
	{
		return mView;
	}

	[[nodiscard]] size_t getWidth() const
	{
		return mWidth;
	}
	[[nodiscard]] size_t getHeight() const
	{
		return mHeight;
	}
	[[nodiscard]] VkFormat getFormat() const
	{
		return mFormat;
	}

private:
	VkImage mNative{VK_NULL_HANDLE};
	VkImageView mView{VK_NULL_HANDLE};
	VmaAllocation mAllocation{VK_NULL_HANDLE};
	VmaAllocationInfo mAllocInfo{};

	size_t mWidth{};
	size_t mHeight{};
	VkFormat mFormat{};

	friend class Device;
};
} // namespace renderer

#endif // RENDERER_IMAGE_HPP
