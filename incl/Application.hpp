#ifndef RENDERER_APPLICATION_HPP
#define RENDERER_APPLICATION_HPP

#include <Context.hpp>
#include <Device.hpp>
#include <Scene.hpp>
#include <Shader.hpp>

#include <glm/glm.hpp>

namespace renderer
{
struct Object
{
	Image* textureRef{nullptr};
	Buffer indices{};
	Buffer vertices{};
	glm::mat4 model;
	size_t indexCount{0};
};

struct UBO
{
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
	Buffer buffer{};
};

class Application
{
public:
	explicit Application(GLFWwindow* window);
	~Application();

	void recreateSwapchain();
	bool render(double deltaSeconds);

private:
	constexpr static uint32_t gPerFrameUploadBufferSize{128 * 1024 * 1024};
	constexpr static VkFormat gDepthFormat{VK_FORMAT_D32_SFLOAT};
	glm::vec3 mUp{0, -1, 0};

	Context mCtx{};

	Device mDevice{};
	Swapchain mSwapchain{};
	Buffer mUploadBuffer{};
	Shader mTriangleShader{};
	std::vector<Image> mRenderTargetImages{};
	std::vector<Image> mDepthImages{};
	VkRenderPass mRenderPass{VK_NULL_HANDLE};
	Pipeline mPipeline{};
	CommandPool mCmdPool{};
	std::vector<Image> mTextures;
	VkFramebuffer mFBO{VK_NULL_HANDLE};
	UBO mUBO{};
	VkDescriptorPool mVertexPool{VK_NULL_HANDLE};
	VkDescriptorPool mFragPool{VK_NULL_HANDLE};
	std::vector<VkDescriptorSet> mDescSets;
	VkSampler mLinearSampler{VK_NULL_HANDLE};
	std::vector<VkSemaphore> mSemaphores;
	std::vector<VkSemaphore> mPresentSemaphores;
	std::vector<VkFence> mFences;
	uint32_t mCurrentPipelineIndex{0};

	std::vector<uint64_t> mSemaphoreIndexCounters;

	SceneData mSceneData{};

	Object mChalet{};

	void createContext(GLFWwindow* window);
	void createDevice();
	void loadScene();
	void createSwapchain();
	void createUploadBuffer();
	void createShader();
	void createRenderTargets();
	void createGraphicsPipeline();
	void createCommandPool();
	void convertImageDataToImages();
	// TODO: Splitting to examples and first one is "vktut" -> renders only single model
	void createChalet();
	void createFramebuffers();
	void createView();
	void createDescriptors();
	void createSampler();
	void record();
	void imageInitBarriers();
	void createFencesAndSemas();
};

} // namespace renderer

#endif // RENDERER_APPLICATION_HPP
