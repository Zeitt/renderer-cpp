#ifndef RENDERER_SWAPCHAIN_HPP
#define RENDERER_SWAPCHAIN_HPP

#include <Image.hpp>
#include <Result.hpp>

#include <vk_mem_alloc.h>
#include <volk.h>

#include <mutex>
#include <vector>

namespace renderer
{
class Swapchain
{
public:
	[[nodiscard]] util::Result<size_t, VkResult> acquireNextImage(VkSemaphore toSignal);

	[[nodiscard]] const Image& getImageRef(size_t index) const;
	[[nodiscard]] size_t getPipelineLength() const;

	[[nodiscard]] VkSwapchainKHR native() const
	{
		return mSwapchain;
	}

private:
	VkSwapchainKHR mSwapchain{VK_NULL_HANDLE};
	std::vector<Image> mImages{};
	size_t mPipelineLength{2};

	VolkDeviceTable mTable{};
	VkDevice mDevice{VK_NULL_HANDLE};

	friend class Device;
};
} // namespace renderer

#endif // RENDERER_SWAPCHAIN_HPP
