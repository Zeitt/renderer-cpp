#ifndef RENDERER_DEVICE_HPP
#define RENDERER_DEVICE_HPP

#include <Buffer.hpp>
#include <CommandBufferPool.hpp>
#include <Image.hpp>
#include <Result.hpp>
#include <Shader.hpp>
#include <Swapchain.hpp>

#include <cstdint>
#include <iostream>
#include <optional>

namespace renderer
{
// Always using imageless extension.
struct FramebufferCreateInfo
{
	VkRenderPass renderPass{VK_NULL_HANDLE};
	uint32_t layers{0};
	VkExtent2D extent{};
	std::vector<VkFramebufferAttachmentImageInfo> imageAttachmentInfos;
};

struct QueueSubmitInfo
{
	std::vector<CommandBuffer>& cmds;
	VkSemaphore semaphore{VK_NULL_HANDLE};
	uint64_t waitValue{UINT64_MAX};
	uint64_t signalValue{UINT64_MAX};
	VkPipelineStageFlags waitFlags{};

	VkFence signalFence{VK_NULL_HANDLE};
};


class Device
{
public:
	// Buffer functions are implemented in Buffer.cpp
	[[nodiscard]] util::Result<Buffer, VkResult> createBuffer(const VkBufferCreateInfo& bufferInfo,
															  const VmaAllocationCreateInfo& allocCreateInfo);
	void destroyBuffer(Buffer& buffer);
	void updateGPUBuffer(CommandBuffer& cmd, Buffer& cpuSrc, Buffer& gpuDst, const VkBufferCopy& region);

	// Shader functions are implemented in Shader.cpp
	[[nodiscard]] util::Result<Shader, VkResult> createShader(const char* vertexFilePath, const char* fragmentFilePath);
	void destroyShader(Shader& shader);

	// Swapchain functions are implemented in Swapchain.cpp
	[[nodiscard]] util::Result<Swapchain, VkResult> createSwapchain(VkSurfaceKHR surface);
	void destroySwapchain(Swapchain&);

	// Image functions are implemented in Image.cpp
	[[nodiscard]] util::Result<Image, VkResult> createImage(const VkImageCreateInfo&, const VkImageViewCreateInfo&,
															const VmaAllocationCreateInfo&);
	[[nodiscard]] util::Result<Image, VkResult> createImageWithStagedData(const VkImageCreateInfo&,
																		  const VkImageViewCreateInfo&,
																		  const VmaAllocationCreateInfo&,
																		  Buffer& staged, const VkBufferImageCopy&,
																		  CommandBuffer&);
	void destroyImage(Image&);

	[[nodiscard]] util::Result<Pipeline, VkResult> createGraphicsPipeline(const Shader&, const VkViewport& viewport,
																		  const VkRect2D& scissor,
																		  VkRenderPass renderPass,
																		  const VkPipelineVertexInputStateCreateInfo&);
	void destroyPipeline(Pipeline&);

	[[nodiscard]] util::Result<VkRenderPass, VkResult> createRenderPass(const VkRenderPassCreateInfo&);
	void destroyRenderPass(VkRenderPass);

	// CommandPool functions are implemented in CommandBufferPool.coo
	[[nodiscard]] util::Result<CommandPool, VkResult> createCommandPool(
		size_t primaryCommandBufferCount, VkCommandPoolCreateFlagBits flagBits = (VkCommandPoolCreateFlagBits) 0);
	void destroyCommandPool(CommandPool& pool);
	[[nodiscard]] VkResult resetCommandPool(CommandPool&, VkCommandPoolResetFlags = 0);

	[[nodiscard]] VkResult queueSubmit(const QueueSubmitInfo&);
	[[nodiscard]] VkResult queuePresent(const VkPresentInfoKHR&);

	[[nodiscard]] uint32_t getQueueIndex() const;

	[[nodiscard]] VkResult waitIdle() const;
	[[nodiscard]] VkResult queueWaitIdle() const;

	[[nodiscard]] util::Result<VkFramebuffer, VkResult> createFramebuffer(const FramebufferCreateInfo&);
	void destroyFramebuffer(VkFramebuffer fbo);

	[[nodiscard]] util::Result<VkDescriptorPool, VkResult> createDescriptorPool(const VkDescriptorPoolCreateInfo&);
	void destroyDescriptorPool(VkDescriptorPool);
	[[nodiscard]] util::Result<std::vector<VkDescriptorSet>, VkResult> allocateDescriptorSets(
		const VkDescriptorSetAllocateInfo&);
	void freeDescriptorSets(VkDescriptorPool pool, std::vector<VkDescriptorSet>&);
	void updateDescriptorSets(const std::vector<VkWriteDescriptorSet>&);

	[[nodiscard]] util::Result<VkSampler, VkResult> createSampler(const VkSamplerCreateInfo&);
	void destroySampler(VkSampler);

	[[nodiscard]] util::Result<VkSemaphore, VkResult> createTimelineSemaphore(size_t initialValue);
	[[nodiscard]] util::Result<VkSemaphore, VkResult> createBinarySemaphore();
	[[nodiscard]] VkResult signalTimelineSemaphore(VkSemaphore, size_t value);
	[[nodiscard]] VkResult waitTimelineSemaphore(VkSemaphore, size_t value, uint64_t nanosecondTimeout = 1'000'000'000);
	void destroySemaphore(VkSemaphore);

	[[nodiscard]] util::Result<VkFence, VkResult> createFence();
	[[nodiscard]] VkResult waitAndResetFence(VkFence fence, bool reset = true);
	[[nodiscard]] VkResult getFenceStatus(VkFence fence);
	void destroyFence(VkFence fence);

	[[nodiscard]] VkDeviceSize getBufferAlignment() const
	{
		return mBufferAlignment;
	}

private:
	VolkDeviceTable mTable{};
	VkDevice mDevice{VK_NULL_HANDLE};
	VkPhysicalDevice mPhysical{VK_NULL_HANDLE};
	VmaVulkanFunctions mVmaFunctions{};
	VmaAllocator mAllocator{VK_NULL_HANDLE};
	VkQueue mMainQueue{VK_NULL_HANDLE};
	uint32_t mQueueFamilyIndex{UINT32_MAX};
	VkDeviceSize mBufferAlignment{0};

	VkResult getFirstPhysicalDevice(VkInstance, VkPhysicalDeviceFeatures2&);
	VkResult createLogicalDevice(const VkDeviceQueueCreateInfo&, const VkPhysicalDeviceFeatures2&);
	VkResult createMemoryAllocator(VkInstance);
	VkResult findQueue(VkDeviceQueueCreateInfo&, VkSurfaceKHR);

	friend class Context;
};
} // namespace renderer

#endif // RENDERER_DEVICE_HPP
