#ifndef RENDERER_SCENE_HPP
#define RENDERER_SCENE_HPP

#include <glm/glm.hpp>
#include <volk.h>

#include <cstdint>
#include <optional>
#include <string>
#include <unordered_set>
#include <vector>

namespace renderer
{
struct MeshData final
{
	std::vector<uint32_t> indices{};
	std::vector<glm::vec3> positions{};
	std::vector<glm::vec3> normals{};
	std::vector<glm::vec2> uvs{};
	std::vector<glm::vec3> tangents{};
};
struct ImageData final
{
	size_t width{0};
	size_t height{0};
	VkFormat format{VK_FORMAT_UNDEFINED};
	size_t nameIndex{SIZE_MAX};
	std::vector<uint8_t> data{};
};
struct MaterialData final
{
	size_t albedoIndex{SIZE_MAX};
	size_t normalIndex{SIZE_MAX};
	size_t metalnessRoughnessIndex{SIZE_MAX};
	size_t nameIndex{SIZE_MAX};
};
struct ObjectData final
{
	size_t transformIndex{SIZE_MAX};
	size_t nameIndex{SIZE_MAX};
	std::vector<size_t> children{};
	std::vector<size_t> meshes{};
	std::vector<size_t> materials{};
};
struct SceneData final
{
	std::vector<MeshData> meshes{};
	std::vector<ImageData> images{};
	std::vector<MaterialData> materials{};
	std::vector<ObjectData> objects{};
	std::vector<glm::mat4> matrices{};
	std::vector<std::string> names{};
};

extern std::optional<SceneData> LoadScene(const char* path);
} // namespace renderer

#endif // RENDERER_SCENE_HPP
