#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;


layout(set = 0, binding = 0) uniform UBO
{
	mat4 model;
	mat4 view;
	mat4 proj;
};


layout(location = 0) out vec2 uvOut;

void main()
{
	gl_Position = proj * view * model * vec4(position, 1.0);
	uvOut = uv;
}
