#version 450

layout(location = 0) in vec2 uv;

layout(set = 1, binding = 0) uniform mediump sampler uSampler;
layout(set = 1, binding = 1) uniform texture2D uTexture;

layout(location = 0) out vec4 outColor;

void main()
{
	vec4 sampled = texture(sampler2D(uTexture,uSampler), uv);
	outColor = vec4(sampled.rgb, 1.0);
	//outColor = vec4(1.0);
}